from database.declaration import Declaration
from moduls.data_loader.DataLoader import commit_changes, save_all_declarations
from moduls.data_loader.ReadJsonFile import find_declaration_by_name_and_file
from moduls.data_loader.ReadJsonFile import get_files_declarations
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QCheckBox, QListWidget, QMessageBox, QPushButton, QTreeWidget, QTreeWidgetItem, QWidget


class LoaderGUI():
    """ LOADER GUI MODULE """

    def __init__(self, tab: QWidget) -> None:
        # DECLARATIONS
        self.tabWidget = tab  # principal tabwidget
        self.treeWidget = tab.findChild(QTreeWidget, 'treeWidget')
        self.uncommitChanges = list()  # will containt all new loaded declarations

        self.data = None  # will containt the data that must be saved
        self.item_clicked = None  # reference to the cliked item from the treewidget
        self.load_files()  # load all json file to the tabwidget
        self.test = 0

        # EVENTS LISTENNERS
        self.buttonLoader = self.tabWidget.findChild(
            QPushButton, 'button_load')
        self.buttonLoader.clicked.connect(lambda: self.load())

        self.buttonCommit = self.tabWidget.findChild(
            QPushButton, 'button_commit')
        self.buttonCommit.clicked.connect(self.commit)

    def add_on_tree_widget(self, data):
        """ Add data on the tablewidget """
        tableWidget = self.get_table_widget()

        row = tableWidget.rowCount()  # index of new Row
        tableWidget.insertRow(row)  # new row creation
        for column, value in enumerate(data.values()):
            cell = QtWidgets.QTableWidgetItem(str(value))
            tableWidget.setItem(row, column, cell)

    def commit(self):
        """ Just commit the data already saved """
        
        if commit_changes(self.uncommitChanges):
            QMessageBox.information(
                self.tabWidget, 'Confirmation du commit', 'Données commitées avec succès')
            self.uncommitChanges.clear()  # drop all items
            self.load_files() # load all saved files
        else:
            QMessageBox.warning(
                self.tabWidget, 'Commit error', 'Erreur lors du commit')

    def load(self):
        """ Save data from form to json file"""

        treeWidget = self.get_tree_widget()

        # get all childs node
        childs = (treeWidget.topLevelItem(i).child(j)
                  for i in range(treeWidget.topLevelItemCount())
                  for j in range(treeWidget.topLevelItem(i).childCount())
                  )

        # checked and not desabled childs
        childs = [child for child in childs if not child.isDisabled()
                  and child.checkState(0)]

        if len(childs) != 0:
            # call the loader module with autocommit option
            autocommit = self.tabWidget.findChild(
                QCheckBox, 'checkBox').isChecked()

            declarations = list()
            for child in childs:
                fileName = child.parent().text(0).replace(' ', '_').lower()  # --> 2020_mars
                date_publication = child.text(
                    0).replace('Date publication: ', '')
                declaration = find_declaration_by_name_and_file(
                    fileName, date_publication)

                # we add Declaration for futur save
                declarations.append(declaration)

            def update_view_list(item: dict, declaration: Declaration) -> None:
                # we need to add item when autocommit is False
                # means that the user will by herself commit all changes, and when he'll
                # do that, we must update the property 'is_loaded' too.
                if not autocommit:
                    # add for futur update (when commit update property 'is_loaded')
                    self.uncommitChanges.append(item)

                # add item like already saved
                self.get_list_widget().addItem(declaration.__str__())

            # save all declarations with commit option
            saved = save_all_declarations(
                declarations, autocommit=autocommit, callback=update_view_list)

            # must be deleted
            if saved:
                if autocommit:
                    msg = "Chargement terminé avec succès"
                    self.load_files()  # reload all files
                else:
                    # we desable all traited childs 
                    for child in childs:
                        child.setDisabled(True)  # disabled child

                    msg = "Chargement temporaire des données terminé. Veiller commiter pour valider"
                QMessageBox.information(
                    self.tabWidget, "Chargement terminé", msg)
            else:
                # add item like already saved
                self.get_list_widget().addItem('#####   ERROR SAVING')
                QMessageBox.warning(
                    self.tabWidget, "Erreur de chargement", "Erreur lors du chargement vers la base de données")

        else:
            QMessageBox.information(self.tabWidget, 'Information',
                                    'Veillez sélectionner une donnée non encore importée')

    def add_top_level_item(self, title):
        """ Add new top level item on the treewidget """

        treeWidget = self.get_tree_widget()
        topLevelItem = treeWidget.topLevelItemCount()
        treeWidget.addTopLevelItem(QTreeWidgetItem(topLevelItem))
        treeWidgetItem = treeWidget.topLevelItem(topLevelItem)
        treeWidgetItem.setText(0, title)

    def add_item(self, value: str, state=False, node: int = None, parent: QTreeWidgetItem = None):
        """ Add new item to given parent node """

        # getting the parent node by its position
        if node is not None:
            parent = self.get_tree_widget().topLevelItem(node)

        # if parent node is
        if parent is not None:
            row = parent.childCount()  # get the number of child
            parent.addChild(QTreeWidgetItem(row))  # create new child
            treeWidgetItem = parent.child(row)  # get created child
            treeWidgetItem.setText(0, value)  # set corresponding text value
            if state:
                treeWidgetItem.setDisabled(True)  # disabled childrue)
                treeWidgetItem.setCheckState(0, 1)
            else:
                treeWidgetItem.setCheckState(0, 0)

    def select_item(self, item: QTreeWidgetItem):
        """ Read the clicked item and charge data """

        try:
            fileName = item.parent().text(0).replace(' ', '_').lower()

            date_publication = item.text(0)  # get the publication date

            date_publication = date_publication.replace('Publication du ', '')
            declaration = find_declaration_by_name_and_file(
                fileName, date_publication)

            # must call and get declaration corresponding to the given date of publication
            # self.data = get_data_from_loader_module
            listWidget = self.get_list_widget()
            listWidget.clear()  # drop all elements

            if listWidget is None:
                return

            keys = ['date_publication',
                    'nombre_tests',
                    'nombre_cas_positifs',
                    'nombre_cas_contacts',
                    'nombre_cas_importes',
                    'nombre_cas_graves',
                    'nombre_patients',
                    'nombre_deces']

            for key in keys:
                listWidget.addItem(
                    f"{key.capitalize().replace('_', ' ')}: {declaration[key]}")

            listWidget.addItem('')
            listWidget.addItem('    |')

            for ville in declaration['villes']:
                listWidget.addItem(f"* Ville: {ville['nom']}")
                for localite in ville['localites']:
                    listWidget.addItem(
                        f"   - {localite['nom']}: {localite['cas']}")

        except AttributeError as _:
            pass  # means the parent node was selected

    def load_files(self):
        """ Load all json file and put them into the tablewidget """

        treeWidget = self.get_tree_widget()
        treeWidget.clear()  # dropping all previous data
        treeWidget.itemDoubleClicked.connect(self.select_item)

        files, data = get_files_declarations()

        # add top level node
        for node, fileName in enumerate(files):
            self.add_top_level_item(fileName)
            # add subnode item elements
            for declaration in data[node]:
                self.add_item(
                    f"Publication du {declaration['date_publication']}", node=node, state=declaration['is_loaded'])

    def get_tree_widget(self) -> QTreeWidget:
        """ Find and return the tree widget """

        return self.tabWidget.findChild(QTreeWidget, 'treeWidget')

    def get_list_widget(self) -> QListWidget:
        """ Find and return the list widget """

        return self.tabWidget.findChild(QListWidget, 'listWidget_3')
