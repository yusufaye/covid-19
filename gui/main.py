from gui.analyser_gui import AnalyserGUI
from gui.explorer_gui import ExplorerGUI
from gui.loader_gui import LoaderGUI
from .acquisition_gui import AcquisitionGUI
from PyQt5 import QtWidgets, uic

class MainGUI():
	""" ALL TABWIDGET ARE HANDLED HERE """

	def __init__(self) -> None:
		app = QtWidgets.QApplication([])
		self.dlg = uic.loadUi('main.ui')

		self.tabwiget_handler(self.dlg.tabWidget)

		self.dlg.show()
		app.exec()

	def tabwiget_handler(self, tabWidget: QtWidgets.QTabWidget):
		for i in range(tabWidget.count()):
			widget = tabWidget.widget(i)
			if widget.objectName() == 'dataAcquisition':
				AcquisitionGUI(widget) # CONTROLLER OF DATA ACQUISITION MODULE
			elif widget.objectName() == 'dataLoader':
				LoaderGUI(widget) # CONTROLLER OF DATA ACQUISITION MODULE
			elif widget.objectName() == 'dataExplored':
				ExplorerGUI(widget) # CONTROLLER OF DATA EXPLORER MODULE
			elif widget.objectName() == 'evolutionAnalyser':
				AnalyserGUI(widget) # CONTROLLER OF DATA ANALYSER MODULE