from tkinter import Frame, Label, W, E

fonts = {
    'normal': 'arial 9',
    'bold': 'arial 9 bold',
}

def create_table(parent, table_title, columns, data, keys, callback=None) -> Frame:
    """ Method use for creating a simple table """
    """
        parent: parent Frame which contents the table
        table_title: title of the final table
        columns: columns headers of table
        data: the content of table
        keys: all keys on data that we must match to columns
        callback: function will call when click event is
    """

    framepage = Frame(parent, bd=1, relief='solid')

    framepage.pack(side="top", fill="both", expand=True)
    labeltitre = Label(framepage, text=table_title,
                       font="arial 20 bold")
    labeltitre.grid(row=0, column=1, columnspan=7, sticky=W+E)

    for x in range(4):
        framepage.grid_columnconfigure(x, weight=1)
    framepage.grid_columnconfigure(5, weight=3)
    framepage.grid_columnconfigure(6, weight=3)
    framepage.config(padx='3.0m', pady='3.0m')

    # Columns of table (headers)
    for i, column in zip(range(len(columns)), columns):
        Label(framepage, text=column, font="Helvetica 10 bold").grid(
            row=1, column=i, sticky=W+E)

    for i, item in zip(range(2, len(data) + 2), data):
        if(i % 2 == 0):
            coulcar = "#000000"
            coulfond = "#729fcf"
        else:
            coulcar = "#000000"
            coulfond = "#babdb6"
        for j, key in zip(range(len(columns)), keys):
            label = Label(framepage, text=item[key], font="helvetica 8 normal",
                          fg=coulcar, bg=coulfond)

            label.grid(row=i, column=j, sticky=W+E)

            def listener(event):
                if event is not None:
                    # We must 
                    return callback(
                        data[event.widget.grid_info()['row'] - 2])

            # We listen each line on the table
            label.bind("<1>", listener)

    return framepage
