import database
from PyQt5 import QtWidgets, uic

class LoginGUI():
    """ GUI INTERFACE FOR CONNEXION USER """

    def __init__(self) -> None:
        self.app = QtWidgets.QApplication([])
        self.dlg = uic.loadUi('login.ui')

        # EVENTS LISTENNERS
        button = self.dlg.button_connexion
        button.clicked.connect(self.connexion)
        self.dlg.show()
        self.app.exec()

    def connexion(self) :
        """ Use for making a simple connection to oracle database """
        login = self.dlg.line_edit_login.text()
        password = self.dlg.line_edit_password.text()

        if database.mysql_connector.MySQLDataBase.connect(login, password):
            self.destroy()
        else:
            self.badcredentials()

    def destroy(self):
        """ Destroy the login component """
        self.dlg.destroy()
        self.app.quit()

    def badcredentials(self):
        """ Show dialog """
        massage_box = QtWidgets.QMessageBox()
        massage_box.warning(self.dlg, 'Echec connexion', "Nom d'utilisateur ou mot de passe incorrect")

