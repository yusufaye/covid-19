from moduls.data_acquisition.HandlerJsonFile import add_declaration
from moduls.data_acquisition.downloadAllPdf import _downloadPdfNew
from moduls.data_loader.ReadJsonFile import get_all_declarations
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QInputDialog, QLineEdit, QListWidget, QListWidgetItem, QMessageBox, QProgressBar, QPushButton, QTreeWidget, QTreeWidgetItem, QWidget, QTableWidget
import traceback
import time
from moduls.data_acquisition.tweeter.TweepyManager import TweepyManager
from moduls.data_acquisition.Downloader import Downloader
from random import randint


class AcquisitionGUI():
    """ ACQUISITION GUI MODULE """

    def __init__(self, tab: QWidget) -> None:
        # DECLARATIONS
        self.tabWidget = tab  # principal tabwidget
        self.form = None  # will containt the form

        # will containt all declartion from the launchAquisition method when it'll be called
        self.declarations = list()

        # initializations
        self.set_max_id()  # initialize index 
        self.update_progressbar('progressBar', None)  # hide progressbar
        self.update_progressbar('progressBarAuto', None)  # hide progressbar
        treeWidget = self.get_tree_widget()
        treeWidget.clear()  # clear treewidget
        treeWidget.itemDoubleClicked.connect(
            self.open_dialog)  # start listeners
        listWidget = self.get_list_widget()
        listWidget.clear()
        listWidget.itemDoubleClicked.connect(self.select_declaration)

        # load all json file to the tabwidget
        self.load_all_saved()

        # EVENTS LISTENNERS
        def load_data(): self.load_data()
        button = self.tabWidget.findChild(QPushButton, 'button_charger')
        button.clicked.connect(load_data)

        button = self.tabWidget.findChild(QPushButton, 'button_sauvegarder')
        button.clicked.connect(self.save)

        button = self.tabWidget.findChild(QPushButton, 'button_charger_auto')
        button.clicked.connect(self.download_auto)

        button = self.tabWidget.findChild(QPushButton, 'button_approuver')
        button.clicked.connect(self.approuve)

    def save(self):
        """ Save data from form to json file"""

        keys = ['date_publication',
                'nombre_tests',
                'nombre_cas_positifs',
                'nombre_cas_contacts',
                'nombre_cas_importes',
                'nombre_cas_graves',
                'nombre_patients',
                'nombre_deces']
        try:
            declaration = dict()
            for key in keys:
                declaration[key] = self.get_line_edit(key)

            treeWidget = self.get_tree_widget()

            declaration['villes'] = list()  # create a new list

            for itemVille in range(treeWidget.topLevelItemCount()):
                topLevelItem = treeWidget.topLevelItem(itemVille)
                ville = {
                    'nom': topLevelItem.text(0),
                    'localites': list()
                }

                for itemLocal in range(topLevelItem.childCount()):
                    child = topLevelItem.child(itemLocal)
                    [local, cas] = child.text(0).replace(' ', '').split(':')
                    # appendadd each localite
                    ville['localites'].append({
                        'nom': local,
                        'cas': cas
                    })

                declaration['villes'].append(ville)  # add ville

            def onExist() -> bool:
                QMessageBox.question(self.tabWidget, "Duplication de données", "La déclration existe déjà. Souhaitez-vous l'écraser ?")
                
                return False

            response = add_declaration(declaration, overwise=onExist)
            # update declacovclsration dict
            if response is True:
                QMessageBox.information(
                    self.tabWidget, 'Sauvegarde', 'Sauvegarde terminée avec succès')

                for (i, item) in enumerate(self.declarations):
                    if item['date_publication'] == declaration['date_publication']:
                        self.declarations.pop(i)
                        self.update_declarations_view()
                        break

            elif response is False:
                QMessageBox.warning(self.tabWidget, 'Erreur',
                                    'Erreur lors de la sauvegarder')

            else:
                pass

            self.load_all_saved()  # update view
            self.set_max_id()  # initialize index 

        except Exception as e:
            print(traceback.print_exc())  # print errors
            raise e

    def add_on_table_widget(self, data):
        """ Add data on the tablewidget """
        tableWidget = self.get_table_widget()

        row = tableWidget.rowCount()  # index of new Row
        tableWidget.insertRow(row)  # new row creation
        for column, value in enumerate(data.values()):
            cell = QtWidgets.QTableWidgetItem(str(value))
            tableWidget.setItem(row, column, cell)

    def select_declaration(self, event: QListWidgetItem) -> dict:
        """ Trait one declaration on list declaration brought by launch declaration method """

        index = None
        date_publication = event.text().split('Du')[1].replace(
            ' ', '')  # output like '2020/02/13'

        for i, declaration in enumerate(self.declarations):
            if declaration['date_publication'] == date_publication:
                index = i
                break

        if index is not None:
            declaration = self.declarations.pop(
                index)  # remove selected declaration
            self.update_formwidget(declaration)

    def load_data(self):
        """ From given url, download, extract and put it in the differents fields """

        line_edit_source = self.tabWidget.findChild(
            QLineEdit, 'line_edit_source')

        # DO SOMTHING TO DOWNLOAD AND EXTRACT DATA FROM DOWNLOADED FILE
        url = QLineEdit.text(line_edit_source).replace(' ', '')
        if not url.startswith('http://') and not url.startswith('https://'):
            QMessageBox.warning(self.tabWidget, 'Mauvaise url',
                                "L'url est incorrect. Veiller la modifier.")
            return

        try:
            progressBar = 'progressBar'
            self.update_progressbar(progressBar, 10)  # 5% done

            def onDownload():
                self.update_progressbar(progressBar, 75)

            # call AcquisitionModule for downloading and extract data from url
            declaration = Downloader.downloadFromUrl(QLineEdit.text(line_edit_source), onDownload=onDownload)
            self.update_formwidget(declaration)

        except Exception as _:
            print(traceback.print_exc())

    def open_dialog(self, item: QTreeWidgetItem):
        """ Use for update the number on the item """

        if item.text(0).count(':') > 0:
            value, ok = QInputDialog.getText(
                self.tabWidget, f"Ville de ", 'Nombre de cas communautaires')

            if ok:
                local = item.text(0).split(':')[0]
                item.setText(0, f"{local}: {value}")

    def load_all_saved(self):
        """ Load all json file and put them into the tablewidget """

        declarations = get_all_declarations()
        tableWidget = self.get_table_widget()
        tableWidget.clear()

        for declaration in declarations:
            data = dict()
            data['date_publication'] = declaration['date_publication']
            data['nombre_tests'] = declaration['nombre_tests']
            data['nombre_cas_positifs'] = declaration['nombre_cas_positifs']
            data['nombre_cas_contacts'] = declaration['nombre_cas_contacts']
            data['nombre_cas_importes'] = declaration['nombre_cas_importes']
            self.add_on_table_widget(data)
            

    def new_top_level_item(self, title) -> QTreeWidgetItem:
        """ Add new top level item on the treewidget """

        treeWidget = self.get_tree_widget()
        topLevelItemCount = treeWidget.topLevelItemCount()
        treeWidget.addTopLevelItem(QTreeWidgetItem(topLevelItemCount))
        treeWidgetItem = treeWidget.topLevelItem(topLevelItemCount)
        treeWidgetItem.setText(0, title)
        return treeWidgetItem

    def add_item(self, value: str, node: int = None, parent: QTreeWidgetItem = None):
        """ Add new item to given parent node """

        # getting the parent node by its position
        if node is not None:
            parent = self.get_tree_widget().topLevelItem(node)

        # if parent node is
        if parent is not None:
            row = parent.childCount()  # get the number of child
            parent.addChild(QTreeWidgetItem(row))  # create new child
            treeWidgetItem = parent.child(row)  # get created child
            treeWidgetItem.setText(0, value)  # set corresponding text value

    def update_formwidget(self, declaration) -> None:
        """ Take an declaration and update values of form """

        progressBar = 'progressBar'

        self.update_progressbar(progressBar, 95)  # 95% done

        keys = ['date_publication',
                'nombre_tests',
                'nombre_cas_positifs',
                'nombre_cas_contacts',
                'nombre_cas_importes',
                'nombre_cas_graves',
                'nombre_patients',
                'nombre_deces']

        for key in keys:
            self.set_line_edit(key, str(declaration[key]))

        self.update_progressbar(progressBar, 100)  # 10% done

        treeWidget = self.get_tree_widget()
        treeWidget.clear()

        for ville in declaration['villes']:
            topLevelItem = self.new_top_level_item(
                str(ville['nom']).capitalize())
            for localite in ville['localites']:
                self.add_item(
                    f"{localite['nom']}: {localite['cas']}", parent=topLevelItem)

        self.update_progressbar(progressBar, None)  # hide

    def download_auto(self):
        """ Try to download from AcquisitionModule and extract data """

        progressBar = 'progressBarAuto'
        self.update_progressbar(progressBar, 0)

        def onFilter(declaration: dict):
            pourcentage = int(self.get_progressbar_widget(progressBar).text().replace('%', ''))

            if declaration is not None:
                self.update_progressbar(progressBar, randint(pourcentage, 100))
                self.declarations.append(declaration)

            else:
                self.update_progressbar(progressBar, None)  # close progressbar
                QMessageBox.information(self.tabWidget, 'Auto-acquisition',
                                        f"L'acquisition automatique est terminée. {len(self.declarations)} fichier(s) téléchargé(s).")

        def onDownloading(tweet: dict):
            if tweet is not None:
                pourcentage = int(self.get_progressbar_widget(progressBar).text().replace('%', ''))
                self.update_progressbar(progressBar, randint(pourcentage, 100))  # close progressbar

            else:
                self.update_progressbar(progressBar, None)  # close progressbar
                print('Download DONE')


        # get the index pages
        max_id = self.get_max_id()
        # start downloading files and extract them
        # launchAcquisitionModule(onFilter, error=error,
        #                        firstPage=firstPage, lastPage=lastPage)

    
        self.declarations = Downloader.downloadAllTweets(max_id=max_id,
            onFilter=onFilter, onDownload=onDownloading)
        
        self.update_declarations_view()


    def update_declarations_view(self):
        """ Update liste of downloaded tweets """
        
        listWidget = self.get_list_widget()
        listWidget.clear()

        for i, declaration in enumerate(self.declarations):
            listWidget.addItem(f"# N°{i + 1}-Du {declaration['date_publication']}")

    def approuve(self):
        """ Save all declaration on mysql database """

        for declaration in self.declarations:
            # update declacovclsration dict
            if not add_declaration(declaration):
                QMessageBox.warning(self.tabWidget, 'Erreur',
                                    'Erreur lors de la sauvegarder')
                return
        
        QMessageBox.information(
            self.tabWidget, 'Sauvegarde', 'Sauvegarde terminée avec succès')

        self.load_all_saved()  # update view
        self.set_max_id()  # initialize index 

    def set_line_edit(self, key, value) -> None:
        """ Update value of given key of line edit """

        self.tabWidget.findChild(QLineEdit, key).setText(str(value))

    def get_line_edit(self, key) -> str:
        """ Get value of given key of line edit """

        return self.tabWidget.findChild(QLineEdit, key).text()

    def set_max_id(self, max_id=None) -> None:
        """ Update value of index pages """
        
        if max_id is None:
            max_id = TweepyManager.last_tweet_id()

        self.set_line_edit('max_id', max_id)

    def get_max_id(self) -> int:
        """ Return tuple containts first and last page """

        max_id = self.get_line_edit('max_id')
        
        try:
            max_id = int(max_id)
            return max_id
        except Exception as _:
            return None

    def get_table_widget(self) -> QTableWidget:
        """ Find and return the table widget """

        return self.tabWidget.findChild(QTableWidget, 'tableWidget')

    def get_tree_widget(self) -> QTreeWidget:
        """ Find and return the tree widget """

        return self.tabWidget.findChild(QTreeWidget, 'treeWidget_2')

    def get_list_widget(self) -> QListWidget:
        """ Find and return the list widget """

        return self.tabWidget.findChild(QListWidget, 'listWidget_2')

    def update_progressbar(self, name: str, value) -> None:
        """ Set percetage """
        if value is None:
            self.get_progressbar_widget(name).setValue(0)
            self.get_progressbar_widget(name).setHidden(True)
        else:
            self.get_progressbar_widget(name).setHidden(False)
            self.get_progressbar_widget(name).setValue(value)

    def get_progressbar_widget(self, name) -> QProgressBar:
        """ Find and return the progressbar widget """

        return self.tabWidget.findChild(QProgressBar, name)
