from datetime import datetime, time, date
from random import randint
from database.ville import Ville
from moduls.data_explorer.explorer import Explorer
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT
from moduls.data_explorer.test import MplCanvas
from gui.card import SENEGAL_CARD, SvgItem, SvgViewer
from PyQt5.QtGui import QImage, QMouseEvent, QPixmap
from database.declaration import Declaration
from database.transmission_communautaire import TransmissionCommunautaire
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QGraphicsView, QLabel, QLineEdit, QListWidget, QListWidgetItem, QMessageBox, QPushButton, QScrollArea, QTreeWidget, QTreeWidgetItem, QWidget, QTableWidget
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

class ExplorerGUI():
    """ EXPLORER GUI MODULE """

    def __init__(self, tab: QWidget) -> None:
        # DECLARATIONS
        self.tabWidget = tab  # principal tabwidget

        self.graphicsSenegal = self.tabWidget.findChild(
            QGraphicsView, 'graphicsView')

        self.viewer = SvgViewer(self.graphicsSenegal, on_click=self.on_click)
        self.viewer.set_svg(SENEGAL_CARD)

        vb_layout = QtWidgets.QVBoxLayout(self.graphicsSenegal)
        vb_layout.addWidget(self.viewer)
        
        
        widget = self.tabWidget.findChild(QWidget, 'widget')
        self.sc = MplCanvas(self.tabWidget, width=5, height=4, dpi=100)
        

        # Create toolbar, passing canvas as first parament, parent (self, the MainWindow) as second.
        self.toolbar = NavigationToolbar(self.sc, widget)

        layout = QtWidgets.QVBoxLayout(widget)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.sc)

    def on_click(self, element: SvgItem):
        """ Use for plot and set details of Contry such as its name and total number of case """

        # @tall
        # call the plot function here as bellow
        # self.plot # if necesairy, add your data when calling

        data = Explorer.get_info_ville(element.title)

        self.load(element.title, data)

        self.plot(element.title, data)

    def plot(self, nom:str, data: list):
        """ Ploter function """

        # widget = self.tabWidget.findChild(QWidget, 'widget')
        # sc = MplCanvas(self, width=5, height=4, dpi=100)
        
        x_values = list()
        y_values = list()
        
        declarations = Declaration.findByVille(nom=nom)
        declarations= sorted(declarations, key=lambda d: d.date_publication)   # sort by date_publication

        for declaration in declarations:
            x_values.append(declaration.date_publication)
            y_values.append(declaration.nombre_cas_positifs)
            
        # for i in range(1, 32):
        #     x_values.append(datetime(2021, 1, i))
        #     y_values.append(randint(0, 100))

        xaxis = [d for d in x_values]
        # xaxis = [str(d) for d in x_values]

        #plot
        self.sc.axes.cla()  # Clear the canvas
        self.sc.axes.set_xticklabels(xaxis, rotation=90)
        self.sc.axes.plot(xaxis, y_values)
        self.sc.axes.scatter(xaxis, y_values, c='r')
        self.sc.draw()

        # Create toolbar, passing canvas as first parament, parent (self, the MainWindow) as second.
        # toolbar = NavigationToolbar(sc, widget)

        # layout = QtWidgets.QVBoxLayout(widget)
        # layout.addWidget(toolbar)
        # layout.addWidget(sc)

    def load(self, nom: str, data: list):
        """ Save data from form to json file"""

        listWidget = self.get_list_widget()
        listWidget.clear()
        listWidget.setWrapping(True)
        listWidget.setDisabled(True)
        listWidget.setAutoFillBackground(True)
        listWidget.setMinimumWidth(300)


        if len(data) > 0:
            lastDeclaration = data[0].declaration

            for t in data:
                if lastDeclaration.date_publication > t.declaration.date_publication:
                    lastDeclaration = t.declaration

            listWidget.addItem(f"""Ville de {nom}
                avec un nombre de cas total égale à {lastDeclaration.nombre_cas_positifs}""")

            listWidget.addItem(f"""Date d'apparition des derniers cas
                est le {lastDeclaration.date_publication}""")

            listWidget.addItem(f"""Répartition des cas à travers
                les différentes localité de {nom}""")

            for local in data:
                listWidget.addItem(
                    f"   * localité de {local.localite.nom} - {local.nombre_cas} cas")
        else:
            listWidget.addItem("Aucune donnée disponible :(")

    def get_list_widget(self) -> QListWidget:
        """ Find and return the list widget """

        return self.tabWidget.findChild(QListWidget, 'listWidget')
