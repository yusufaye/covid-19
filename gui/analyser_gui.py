from datetime import datetime
from moduls.evolution_analyser.Progression import ArrowShape, Progression, generateProgressionJson, propagationsAnalyser
from PyQt5 import QtSvg, QtWidgets
from PyQt5.QtWidgets import QGraphicsView, QLineEdit, QListWidget, QPushButton, QWidget


class AnalyserGUI():
    """ ANALYSER GUI MODULE """

    def __init__(self, tab: QWidget) -> None:
        # DECLARATIONS
        self.tabWidget = tab  # principal tabwidget
        self.data = None

        self.graphicsView: QGraphicsView = self.tabWidget.findChild(
            QGraphicsView, 'graphicsView_2')
            
        self.load()

        def load(): self.load()
        button = self.tabWidget.findChild(QPushButton, 'pushButton')
        button.clicked.connect(load)

    def load(self):
        value: str = str(self.tabWidget.findChild(QLineEdit, 'lineEdit').text())

        if value is not None:
            value = value.split('/')
            if len(value) == 2:
                year, month = int(value[0]), int(value[1])
                _date = datetime(year, month, 1)
            else:
                _date = datetime.today()
        else:
            _date = datetime.today()

        transmission_level = Progression(_date)

        self.data = generateProgressionJson(transmission_level)

        if ArrowShape(self.data):
            self.show(path='./moduls/evolution_analyser/progression.svg')
        else:
            self.show(path='./moduls/evolution_analyser/senegal_dep.svg')


    def show(self, path):
        svgWidget = QtSvg.QSvgWidget(path)
        svgWidget.setGeometry(50, 50, 759, 668)

        # self.graphicsView.c
        vb_layout = QtWidgets.QVBoxLayout(self.graphicsView)
        vb_layout.addWidget(svgWidget)
        
        listWidget = self.tabWidget.findChild(QListWidget, 'listWidget_4')
        listWidget.clear()
        # propagations = propagationsAnalyser()
        
        for prop in self.data:
            listWidget.addItem(f"{prop['__start']} |----> {prop['__end']}")
        
        