from .mysql_connector import MySQLDataBase

class Ville():
    def __init__(self, nom='') -> None:
        self.id = None
        self.nom = nom.capitalize()

    def save(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            if self.id is None:
                cursor.execute(f"insert into covid.ville(nom) values('{self.nom}')")
                cursor.execute("select id from covid.ville order by id desc limit 1")
                self.id, = cursor.fetchone() # update id property
            else:
                cursor.execute(f"update table covid.ville set nom='{self.nom}' where id={self.id}")

    def delete(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('delete covid.ville where id=:d', [self.id])
    
    @classmethod
    def query(cls):
        villes = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('select * from covid.ville')
            keys = map(lambda x: x[0], cursor.description)
            for response in cursor.fetchall():
                ville = Ville()
                ville.dump(response, keys)
                villes.append(ville)

        return villes
    
    @classmethod
    def find(cls, id):
        ville = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute(f'select * from covid.ville v where id={id}')
            response = cursor.fetchone()
            keys = map(lambda x: x[0], cursor.description)
            if response is not None:
                ville = Ville()
                ville.dump(response, keys)
                
        return ville

    @classmethod
    def find_by_nom(cls, nom: str):
        ville = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute(f"select * from covid.ville v where nom='{nom}'")
            response = cursor.fetchone()
            keys = map(lambda x: x[0], cursor.description)
            if response is not None:
                ville = Ville()
                ville.dump(response, keys)
                
        return ville

    def dump(self, response, keys):
        """ Mapper tupe to object instance """

        for key, value in zip(keys, response):
            self.__setattr__(key.lower(), value)

    def __str__(self) -> str:
        return f"""ville = [
        'id': {self.id},
        'nom': {self.nom},
        ]"""
