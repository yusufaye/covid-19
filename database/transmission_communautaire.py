from datetime import datetime
from .mysql_connector import MySQLDataBase
from .declaration import Declaration
from .localite import Localite


class TransmissionCommunautaire():
    def __init__(self, nombre_cas=0, declaration:Declaration=None, localite:Declaration=None) -> None:
        self.id = None
        self.nombre_cas = nombre_cas
        self.declaration = declaration
        self.localite = localite

    def save(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            if self.id is None:
                cursor.execute(f"""insert into covid.transmission_communautaire(nombre_cas, declaration_id, localite_id)
                    values({self.nombre_cas}, {self.declaration.id}, {self.localite.id})""")
                cursor.execute(
                    "select id from covid.transmission_communautaire order by id desc limit 1")
                self.id, = cursor.fetchone()  # update id property
            else:
                cursor.execute(f"""update table covid.transmission_communautaire set
                    nombre_cas={self.nombre_cas}
                    declaration_id={self.declaration.id}
                    localite_id={self.localite.id}
                    where id={self.id}""")

    def delete(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute(
                'delete covid.transmission_communautaire where id=:d', [self.id])

    @classmethod
    def query(cls):
        transmissions = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = """select t.*,
                d.id as localite_id, d.nom as localite_nom
                from (select t.id, t.nombre_cas, t.localite_id,
                        d.id as declaration_id,  
                        d.nom as declaration_nom,  
                        d.url as declaration_url,  
                        d.date_publication as declaration_date_publication,     
                        d.nombre_tests as declaration_nombre_tests,  
                        d.nombre_cas_positifs as declaration_nombre_cas_positifs,   
                        d.nombre_cas_contacts as declaration_nombre_cas_contacts,   
                        d.nombre_cas_importes as declaration_nombre_cas_importes,   
                        d.nombre_cas_graves as declaration_nombre_cas_graves,   
                        d.nombre_patients as declaration_nombre_patients,   
                        d.nombre_deces as declaration_nombre_deces   
                    from covid.transmission_communautaire t join covid.declaration d on t.declaration_id=d.id) t
                    join covid.localite d on t.localite_id=d.id
                """

            cursor.execute(sql)

            for response in cursor.fetchall():
                keys = map(lambda x: x[0].lower(), cursor.description)
                cas = TransmissionCommunautaire()
                cas.dump(response, keys)
                transmissions.append(cas)

        return transmissions

    @classmethod
    def queryWithLocalite(cls, localite_id: int):
        cas = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""select t.*,
                d.id as localite_id, d.nom as localite_nom
                from (select t.id, t.nombre_cas, t.localite_id,
                        d.id as declaration_id,  
                        d.nom as declaration_nom,  
                        d.url as declaration_url,  
                        d.date_publication as declaration_date_publication,     
                        d.nombre_tests as declaration_nombre_tests,  
                        d.nombre_cas_positifs as declaration_nombre_cas_positifs,   
                        d.nombre_cas_contacts as declaration_nombre_cas_contacts,   
                        d.nombre_cas_importes as declaration_nombre_cas_importes,   
                        d.nombre_cas_graves as declaration_nombre_cas_graves,   
                        d.nombre_patients as declaration_nombre_patients,   
                        d.nombre_deces as declaration_nombre_deces     
                    from covid.transmission_communautaire t
                            join covid.declaration d on t.localite_id={localite_id} and t.declaration_id=d.id
                        order by d.date_publication desc limit 1) t
                    join covid.localite d on t.localite_id=d.id
                """

            cursor.execute(sql)
            response = cursor.fetchone()
            keys = map(lambda x: x[0].lower(), cursor.description)

            if response is not None:
                cas = TransmissionCommunautaire()
                cas.dump(response, keys)

        return cas


    @classmethod
    def cumul_by_year_and_month(cls, nom: str, year: str, month: str) -> int:
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""
                select sum(nombre_cas) as cumul from
                    (select l.id, l.nom, t.declaration_id, t.nombre_cas from
                        (select id, nom from covid.localite where nom like '%{nom}%') as l
                            join covid.transmission_communautaire as t where l.id=t.localite_id) as s
                                join covid.declaration d on s.declaration_id=d.id
                                    where date_publication like '{year}-%{month}-%';
            """
            cursor.execute(sql)
            response, = cursor.fetchone()
            if response is None:
                return 0
            else:
                return response

    @classmethod
    def last_date_appear(cls, nom: str) -> datetime:
        """"""
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""
                select date_publication from
                    (select declaration_id from
                        (select id, nom from covid.localite where nom like '%{nom}%') l
                        join covid.transmission_communautaire t on l.id=t.localite_id
                    ) sr
                    join covid.declaration d on d.id=sr.declaration_id order by date_publication desc limit 1;
                """
            try:
                response = cursor.fetchone()

                if response is not None:
                    return response
            except Exception as _:
                pass

        return datetime.today()


    @classmethod
    def find(cls, id):
        cas = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""select t.*,
                d.id as localite_id, d.nom as localite_nom
                from (select t.id, t.nombre_cas, t.localite_id,
                        d.id as declaration_id,  
                        d.nom as declaration_nom,  
                        d.url as declaration_url,  
                        d.date_publication as declaration_date_publication,     
                        d.nombre_tests as declaration_nombre_tests,  
                        d.nombre_cas_positifs as declaration_nombre_cas_positifs,   
                        d.nombre_cas_contacts as declaration_nombre_cas_contacts,   
                        d.nombre_cas_importes as declaration_nombre_cas_importes,   
                        d.nombre_cas_graves as declaration_nombre_cas_graves,   
                        d.nombre_patients as declaration_nombre_patients,   
                        d.nombre_deces as declaration_nombre_deces     
                    from covid.transmission_communautaire t join covid.declaration d on t.id={id} and t.declaration_id=d.id) t
                    join covid.localite d on t.localite_id=d.id
                """

            cursor.execute(sql)
            response = cursor.fetchone()
            keys = map(lambda x: x[0].lower(), cursor.description)

            if response is not None:
                cas = TransmissionCommunautaire()
                cas.dump(response, keys)

        return cas

    def dump(self, response, keys):
        """ Mapper tupe to object instance """

        localite = Localite()
        declaration = Declaration()
        for key, value in zip(keys, response):
            if not str(key).startswith('localite'):
                self.__setattr__(key, value)
            elif str(key).startswith('localite'):
                localite.__setattr__(str(key).replace('localite_', ''), value)
            elif str(key).startswith('declaration'):
                declaration.__setattr__(
                    str(key).replace('declaration_', ''), value)

        self.declaration = declaration
        self.localite = localite

    def __str__(self) -> str:
        return f"""transmission_communautaire = [
        'id': {self.id},
        'nombre_cas': {self.nombre_cas},
        'declaration': {self.declaration.__str__()},
        'localite': {self.localite.__str__()}
        ]"""
