-- THIS IS SQL FILE USED FOR CREATE THE USER AND SCHEMA

-- DROPPING USERS
DROP user if EXISTS acquisition@localhost ;
DROP user if EXISTS loader@localhost ;
DROP user if EXISTS explorer@localhost ;
DROP user if EXISTS analyser@localhost ;

-- CREATE EACH USERS
CREATE user acquisition@localhost identified by 'passer' ;
CREATE user loader@localhost identified by 'passer' ;
CREATE user explorer@localhost identified by 'passer' ;
CREATE user analyser@localhost identified by 'passer' ;

-- CREATE NEW DATABASE
DROP DATABASE if EXISTS covid ;
CREATE DATABASE covid ;

-- FOR LOADER
GRANT create, SELECT, INSERT, UPDATE, DELETE on covid.* to loader@localhost ;

-- FOR ANALYSER
GRANT SELECT on covid.* to analyser@localhost ;

-- FOR EXPLORER
GRANT SELECT on covid.* to explorer@localhost ;