-- USER DATABASE
USE covid ;


-- DROP TABLES IF ALREADY EXISTS

DROP TABLE if EXISTS transmission_communautaire cascade ;
DROP TABLE if EXISTS localite cascade ;
DROP TABLE if EXISTS ville cascade ;
DROP TABLE if EXISTS declaration cascade ;


-- CREATE ALL TABLES

CREATE TABLE declaration(
    id int primary key auto_increment,
    nom varchar(255) not null,
    url varchar(255),
    date_publication date UNIQUE not null,
    nombre_tests int not null,
    nombre_cas_positifs int not null,
    nombre_cas_contacts int not null,
    nombre_cas_importes int not null,
    nombre_cas_graves int not null,
    nombre_patients int not null,
    nombre_deces int not null
) ENGINE=InnoDB ;

CREATE TABLE transmission_communautaire(
    id int primary key auto_increment,
    nombre_cas int not null,
    declaration_id int not null,
    localite_id int not null
) ENGINE=InnoDB ;

CREATE TABLE ville(
    id int primary key auto_increment,
    nom varchar(255) not null UNIQUE
) ENGINE=InnoDB ;

CREATE TABLE localite(
    id int primary key auto_increment,
    nom varchar(255) UNIQUE not null,
    ville_id int NOT NULL
) ENGINE=InnoDB ;

-- CREATE CONSTRAINTS CONSTRAINTS
ALTER TABLE localite add constraint fk_localite_ville foreign key (ville_id) references ville(id) ON DELETE CASCADE;
ALTER TABLE transmission_communautaire add constraint fk_tc_localite foreign key (localite_id) references localite(id) ON DELETE CASCADE;
ALTER TABLE transmission_communautaire add constraint fk_tc_declaration foreign key (declaration_id) references declaration(id) ON DELETE CASCADE;
