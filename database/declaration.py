from .mysql_connector import MySQLDataBase
import datetime


class Declaration():
    def __init__(self, nom='', url='', nombre_tests=0, nombre_cas_positifs=0, nombre_cas_contacts=0, nombre_cas_importes=0, nombre_cas_graves=0,
                 nombre_patients=0, nombre_deces=0, date_publication=datetime.date.today()) -> None:
        self.id = None
        self.date_publication = date_publication
        self.nom = nom
        self.url = url
        self.nombre_tests = nombre_tests
        self.nombre_cas_positifs = nombre_cas_positifs
        self.nombre_cas_contacts = nombre_cas_contacts
        self.nombre_cas_importes = nombre_cas_importes
        self.nombre_cas_graves = nombre_cas_graves
        self.nombre_patients = nombre_patients
        self.nombre_deces = nombre_deces

    def save(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            if self.id is None:
                print(f"""insert into covid.declaration(nom, url, date_publication, nombre_tests,nombre_cas_positifs, nombre_cas_contacts, nombre_cas_importes,nombre_cas_graves, nombre_patients, nombre_deces)
                        values('Declaration du 2021 4 13','https://sante.sec.gouv.sn','2021/4/13',0,0,0,0,0,0,0);""")
                cursor.execute(f"""insert into covid.declaration(
                        nom, url, date_publication, nombre_tests,
                        nombre_cas_positifs, nombre_cas_contacts, nombre_cas_importes,
                        nombre_cas_graves, nombre_patients, nombre_deces)
                        values('{self.nom}',
                            '{self.url}',
                            '{self.date_publication}',
                            {self.nombre_tests},
                            {self.nombre_cas_positifs},
                            {self.nombre_cas_contacts},
                            {self.nombre_cas_importes},
                            {self.nombre_cas_graves},
                            {self.nombre_patients},
                            {self.nombre_deces}
                        )""")
                cursor.execute(
                    "select id from covid.declaration order by id desc limit 1")
                self.id, = cursor.fetchone()  # update id property
            else:
                cursor.execute(f"""update table covid.declaration set nom='{self.nom}',
                        url='{self.url}',
                        date_publication='{self.date_publication}',
                        nombre_tests={self.nombre_tests},
                        nombre_cas_positifs={self.nombre_cas_positifs},
                        nombre_cas_contacts={self.nombre_cas_contacts},
                        nombre_cas_importes={self.nombre_cas_importes},
                        nombre_cas_graves={self.nombre_cas_graves},
                        nombre_patients={self.nombre_patients},
                        nombre_deces={self.nombre_deces}
                        where id={self.id}
                    """)

    def delete(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('delete covid.declaration where id=:d', [self.id])

    @classmethod
    def query(cls):
        declarations = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('select * from covid.declaration')
            for response in cursor.fetchall():
                declaration = Declaration()
                declaration.dump(response, cursor.description)
                declarations.append(declaration)

        return declarations
    
    @classmethod
    def findByVille(cls, nom: str):
        declarations = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""select d.* from covid.declaration d where d.id in
                (select declaration_id from covid.transmission_communautaire t where t.localite_id in
                    (select id from covid.localite where ville_id =
                        (select id from covid.ville where nom like '%{nom}%')))
                order by d.date_publication desc
            """
            
            cursor.execute(sql)
            
            for response in cursor.fetchall():
                declaration = Declaration()
                declaration.dump(response, cursor.description)
                declarations.append(declaration)

        return declarations

    @classmethod
    def find(cls, id):
        declaration = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('select * from covid.declaration d where')
            response = cursor.fetchone()
            if response is not None:
                declaration = Declaration()
                declaration.dump(response, cursor.description)

        return declaration

    def dump(self, response, description):
        """ Mapper tupe to object instance """

        keys = map(lambda x: x[0], description)
        for key, value in zip(keys, response):
            self.__setattr__(key.lower(), value)

    def __str__(self) -> str:
        return f"""declaration = [
        'id': {self.id},
        'date_publication': {self.date_publication},
        'nom': {self.nom},
        'url': {self.url},
        'nombre_tests': {self.nombre_tests},
        'nombre_cas_positifs': {self.nombre_cas_positifs},
        'nombre_cas_contacts': {self.nombre_cas_contacts},
        'nombre_cas_importes': {self.nombre_cas_importes},
        'nombre_cas_graves': {self.nombre_cas_graves},
        'nombre_patients': {self.nombre_patients},
        'nombre_deces': {self.nombre_deces},
        ]"""
