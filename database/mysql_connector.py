from gui.login import LoginGUI
import json
from mysql import connector
from mysql.connector.connection import MySQLConnection


ACQUISITION = 'ACQUISITION'
LOADER = 'LOADER'
EXPLORER = 'EXPLORER'
ANALYSER = 'ANALYSER'


class MySQLDataBase():
    """ ORACLE DATA CONNECTION """

    db = None  # global instance of database
    role = None  # the role of user

    @classmethod
    def get_connexion(cls):
        if cls.db is None:
            if cls.get_config_property('initialized') is False:
                cls.initialize_db(True)
            elif cls.get_config_property('initialized') is True:
                LoginGUI()
        else:
            return cls.db

    @classmethod
    def auth(cls):
        """ Authentification of USER """

        # calling login interface
        LoginGUI()

    @classmethod
    def config_connection(cls):
        with open('config.json', 'r') as file:
            config = json.load(file)
            config_db = config['app']['db']
            host = config_db['host']
            port = config_db['port']
            return (host, port)

    @classmethod
    def get_user_config(cls):
        """ Return a tuple of user and password from config.jason file """

        with open('config.json', 'r') as file:
            config = json.load(file)
            config_db = config['app']['db']
            user = config_db['user']
            password = config_db['password']
            return user, password

    @classmethod
    def get_text_from_file(cls, path) -> str:
        """ Return the content of the specified file as a string """

        with open(str(path), 'r') as file:
            return file.read()

    @classmethod
    def update_config_property(cls, property, value):
        # Open the file with r mode
        with open('config.json', 'r') as file:
            config = json.load(file)
            config['app'][property] = value

        # open with w mode
        with open('config.json', 'w') as file:
            json.dump(config, file, indent=2)

    @classmethod
    def get_config_property(cls, property):
        with open('config.json', 'r') as file:
            config = json.load(file)['app']
            return config[property]

    @classmethod
    def connect(cls, user: str, password: str) -> bool:
        """ Use for making a simple connection to oracle database """

        try:
            print("##### TRY TO CONNECT")

            (host, port) = cls.config_connection() # get the host and port for make a connexion

            cls.db = connector.connect(
                host=host,
                port=port,
                user=user,
                password=password)
                
            cls.db.autocommit = False

            print("##### CONNECTION SUCCESSFULLY")

            MySQLDataBase.role = user.upper()  # save upper username as role

            return True
        except Exception as e:
            print(e)
            return False

    @classmethod
    def connect_as_root(cls):
        """ Use for making a connection to oracle database as a sysdba"""

        print("##### TRY TO CONNECT ")
        user, password = cls.get_user_config()
        cls.db = connector.connect(user=user, password=password)
        
        cls.db.autocommit = False
        
        print("##### CONNECTION SUCCESSFULLY")

    @classmethod
    def initialize_db(cls):
        """ Used for generete or regenerate schema """

        # App already configured
        if cls.get_config_property('initialized') is True:
            return

        cls.connect_as_root()
        with cls.db.cursor() as cursor:
            print('----> START TO GENERATE SCHEMAS <----')
            global_schema = cls.get_text_from_file(
                'database/schemas/schema.sql').split(';')
            tables_schema = cls.get_text_from_file(
                'database/schemas/table-schema-relational.sql').split(';')

            print('----> GENERATE GLOBAL SCHEMA')
            for sql_command in global_schema:
                # have more than 2 caracters
                if len(sql_command) > 2:
                    try:
                        print(sql_command)  # show sql command
                        cursor.execute(sql_command)
                    except Exception as e:
                        print('>--- error', e)

            print('----> GENERATE TABLE SCHEMA')
            for sql_command in tables_schema:
                # have more than 2 caracters
                if len(sql_command) > 2:
                    try:
                        print(sql_command)  # show sql command
                        cursor.execute(sql_command)
                    except Exception as e:
                        print('>--- error', e)
            print('----> SCHEMAS ARE BEEN GENERATED SUCCESSFULLY <----')

            cls.update_config_property('initialized', True)

        # Disconnecting to oracle database
        cls.disconnect()

    @classmethod
    def disconnect(cls):
        cls.db = None
