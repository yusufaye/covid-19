from .mysql_connector import MySQLDataBase
from .ville import Ville


class Localite():
    def __init__(self, nom='', ville: Ville = None) -> None:
        self.id = None
        self.nom = nom.capitalize()
        self.ville = ville

    def save(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            if self.id is None:
                cursor.execute(
                    f"insert into covid.localite(nom, ville_id) values('{self.nom}', {self.ville.id})")
                cursor.execute(
                    "select id from covid.localite order by id desc limit 1")
                self.id, = cursor.fetchone()  # update id property
            else:
                cursor.execute(
                    f"update table covid.localite set nom='{self.nom}' ville_id={self.ville.id} where id={self.id}")

    def delete(self):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute('delete covid.localite where id=:d', [self.id])

    @classmethod
    def query(cls):
        localites = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute("""select d.id, d.nom, r.id as ville_id, r.nom as ville_nom 
                from covid.localite d join covid.ville r on d.ville_id=r.id""")
            for response in cursor.fetchall():
                keys = map(lambda x: x[0].lower(), cursor.description)
                localite = Localite()
                localite.dump(response, keys)
                localites.append(localite)

        return localites

    @classmethod
    def find(cls, id):
        localite = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute(f"""select d.id, d.nom, r.id as ville_id, r.nom as ville_nom
                from (select * from covid.localite d where d.id={id}) d
                join covid.ville r on d.ville_id=r.id""")

            response = cursor.fetchone()
            keys = map(lambda x: x[0].lower(), cursor.description)

            if response is not None:
                localite = Localite()
                localite.dump(response, keys)

        return localite

    @classmethod
    def find_by_nom(cls, nom: str):
        localite = None
        with MySQLDataBase.get_connexion().cursor() as cursor:
            cursor.execute(f"""select d.id, d.nom, r.id as ville_id, r.nom as ville_nom
                from (select * from covid.localite d where d.nom='{nom}') d
                join covid.ville r on d.ville_id=r.id""")

            response = cursor.fetchone()
            keys = map(lambda x: x[0].lower(), cursor.description)

            if response is not None:
                localite = Localite()
                localite.dump(response, keys)

        return localite

    def dump(self, response, keys):
        """ Mapper tupe to object instance """

        ville = Ville()
        for key, value in zip(keys, response):
            if not str(key).startswith('ville'):
                self.__setattr__(key, value)
            elif str(key).startswith('ville'):
                ville.__setattr__(str(key).replace('ville_', ''), value)

        self.ville = ville

    @classmethod
    def findLocaliteByVille(cls, title):
        localites = list()
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""select l.id, l.nom, v.id as ville_id, v.nom as ville_nom from
                (select id, nom from covid.ville where nom like '%{title}%') as v join covid.localite as l
                on l.ville_id=v.id"""

            cursor.execute(sql)

            for response in cursor.fetchall():
                keys = map(lambda x: x[0].lower(), cursor.description)
                localite = Localite()
                localite.dump(response, keys)
                localites.append(localite)

        return localites

    @classmethod
    def test(cls):
        with MySQLDataBase.get_connexion().cursor() as cursor:
            sql = f"""select s.id, s.nom, sum(nombre_cas) from 
                (select l.id, l.nom, t.declaration_id, t.nombre_cas from 
                (select id, nom from covid.localite where nom like '%Thies%') 
                as l join covid.transmission_communautaire as t where l.id=t.localite_id) 
                as s join covid.declaration d on s.declaration_id=d.id where date_publication like '2021-03-%'"""

            cursor.execute(sql)

            print(cursor.fetchone())

    def __str__(self) -> str:
        return f"""localite = [
        'id': {self.id},
        'nom': {self.nom},
        'ville': {self.ville.__str__()}
        ]"""
