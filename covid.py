# This is the entry point

from gui.main import MainGUI
from database.mysql_connector import MySQLDataBase
import json

# START POINT
if __name__ == '__main__':
    print('##################### COVID-19 #######################')
    with open("config.json", 'r') as file:
        config = json.load(file)['app']
        print('Application name', config['name'])
        print('Version', config['version'])
        print('##################### STARTED  ########################')
    
    # initialize the database if not yet done
    MySQLDataBase.initialize_db()
    
    # @yusufaye must catch calling auth in loop
    # start authentification
    # while MySQLDataBase.db is None: # make sure that the user is authenticated
    # MySQLDataBase.auth()
    
    MySQLDataBase.connect_as_root()
    
    MainGUI() # start main app


