FROM ubuntu:latest

ADD . /
ADD moduls /moduls
ADD gui /gui
ADD database /database

RUN apt-get -y  update
RUN apt-get install apt-utils -y
RUN apt-get install -y mysql-server
RUN apt-get install -y libmysqlclient-dev
RUN apt-get install -y python3-pip
RUN apt-get install -y build-essential libssl-dev libffi-dev python3-dev
RUN apt-get install -y xauth
RUN apt-get install -y libgl1-mesa-glx
RUN apt-get install -y python3-pyqt5.qtsvg
RUN apt-get install -y --reinstall libxcb-xinerama0
RUN pip3 install --upgrade pip
RUN pip3 install -r dependencies.txt

EXPOSE 3306
EXPOSE 3307

RUN apt-get install nano
RUN export QT_DEBUG_PLUGINS=1
RUN apt-get install -y x11-apps
RUN chmod 755 docker.sql

CMD [ "bash" ]
