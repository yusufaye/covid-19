# la concentration du cumul de nombre de cas sur la population du département : Conc
# le taux de progression de ce cumul mensuel par rapport au mois précédent (Prog)
# create une table population du departement ak comme key foreign dep_id
# create une table contenant la conc de chaque departement ak comme key foreign dep_id

from database.transmission_communautaire import TransmissionCommunautaire
import datetime
import svgwrite
import json
from PyQt5 import QtCore
from moduls.evolution_analyser.Functions import *


def Progression(date=datetime.date.today()) -> list:
    taux_progression = list()
    transmission_level = list()

    # taux de progression
    # date today

    with open('./moduls/evolution_analyser/Population.json', 'r') as f:
        pop = json.load(f)['POPULATIONS']
        for i, dept in enumerate(DEPARTEMENTS):
            # chaque deprt on calcule la taux de progression mois i et mois i-1
            conc1 = TransmissionCommunautaire.cumul_by_year_and_month(
                dept, date.year, date.month)

            if conc1 > 0:
                conc2 = TransmissionCommunautaire.cumul_by_year_and_month(
                    dept, date.year, date.month - 1)
                cmounth = (i, dept, float((conc1 - conc2) / pop[dept.upper()]))

                # [(index, departement, taux), ...]
                taux_progression.append(cmounth)

    # niveau de transmissibiliter
    # calcule le niveau de transmissibilter de chaque element par rapport au autre departement
    # tout en ignorant celui du departement meme

    for i, departement, taux in taux_progression:
        defaulttranslevelList = list()
        for j in range(len(DEPARTEMENTS)):
            if i != j:
                # dept i against depart j
                Tlevel = (i, j, taux / GiveDistance(i + 1, j + 1))
                # (depart, arriv, level)
                defaulttranslevelList.append(Tlevel)

        # connaitre qui a contamine qui
        transmission_level.append(levelTransSup(defaulttranslevelList))

    # for i in range(len(DEPARTEMENTS)):
    #     defaulttranslevelList = list()
    #     for j in range(len(DEPARTEMENTS)):
    #         if i != j:
    #             # dept i against depart j
    #             Tlevel = (i, j, taux_progression[j][1]
    #                       / GiveDistance(i + 1, j + 1))
    #             # (depart, arriv, level)
    #             defaulttranslevelList.append(Tlevel)

    #     # connaitre qui a contamine qui
    #     transmission_level.append(levelTransSup(defaulttranslevelList))

    return transmission_level


""" draw arrow shape"""


def ArrowShape(data = None) -> bool:
    # prend un dictionnaire de centre des region et a partir du file Progession.json
    # schematise le progression pas les fleche.
    """add Map"""
    dwg = svgwrite.Drawing('./moduls/evolution_analyser/progression.svg', id="svg22",
                           size=("631.36488", "554"))

    dwg.viewbox(0, 0, 611.36488, 444.75151)

    css = 'land { fill: #CCCCCC;fill-opacity: 1;stroke:white;stroke-opacity: 1;stroke-width:0.5; } @keyframes anim-nw-cell {0% { opacity: 1} 100% {opacity: 0}} nw-cell {animation: 1s ease infinite anim-nw-cell}'
    dwg.defs.add(dwg.style(css, id="style"))

    #css2='nw-cell {animation: 1s ease infinite anim-nw-cell}'
    # dwg.defs.add(dwg.style(css2,id="style"))

    marker = dwg.marker(insert=(5, 5), size=(10, 10), id="arrowhead",
                        orient="auto", markerWidth="5", markerHeight="7", refX="0", refY="3.5")
    marker.add(dwg.polyline([(0, 0), (5, 5.2), (0, 9)],
                            stroke='#f10000', fill='none'))
    dwg.defs.add(marker)

    dwg.add(dwg.text('Progression Covid-19 au Sénégal',
                     insert=(10, -10),
                     stroke='#500',
                     fill='#000000',
                     stroke_width=0.5,
                     font_size='36px',
                     font_weight="bold",
                     font_family="Arial Courier New")
            )
    # add path departement
    CreateAll_Group_Map(dwg)
    # """ add arrow Shape"""
    gline = svgwrite.container.Group(id='line')  # group for arrow line
    
    if data is None:
        with open('./moduls/evolution_analyser/progression.json', 'r') as f:
            propag = json.load(f)['propagations']
    else:
        propag = data
    i = 1
    center1 = QtCore.QPointF(0, 0)
    center2 = QtCore.QPointF(0, 0)
    for c in propag:
        depart: str = c['__start'].capitalize()
        fin = c['__end'].capitalize()
        # print(depart)
        # print("-----")
        # print(fin)
        with open('./moduls/evolution_analyser/Coordinate_Center.json') as ff:
            Allparams = json.load(ff)
            for first_center in Allparams[depart]:
                center1 = QtCore.QPointF(
                    first_center['x'], first_center['y'])

            for second_center in Allparams[fin]:
                center2 = QtCore.QPointF(
                    second_center['x'], second_center['y'])

            # print(center1)
            x1 = center1.x()
            y1 = center1.y()
            x2 = center2.x()
            y2 = center2.y()
            # adding to svg
            item_line = "line"+str(i)
            # gline()
            gline.add(dwg.line((x1, y1), (x2, y2), id=item_line, stroke="#0f0f11")

                        ).set_markers((None, False, marker))
            # search comment ajouter une class a une path de svg avec svgwrite.
            i += 1

    dwg.add(gline)
    # write svg file to disk
    dwg.save()

    return True


# ArrowShape()
# ----------------------------------------------------------------------------------------------------------------

def generateProgressionJson(leveltrans) -> list:
    """Methode qui creer le json file au format exploitable"""
    # filejson={"propagations":[{}]}
    filejson = {"propagations": []}
    for end, source, taux in leveltrans:
        if taux > 0:
            date_appear = TransmissionCommunautaire.last_date_appear(source)
            amout_case = TransmissionCommunautaire.cumul_by_year_and_month(
                source, date_appear.year, '%')
            # date_appear = getdatefirstCaseInDept(source)

            filejson["propagations"].append({
                "__date_appear": str(date_appear),
                "__start": DEPARTEMENTS[source - 1].capitalize(),
                "__end": DEPARTEMENTS[end - 1].capitalize(),
                "__amount_case": amout_case,
            })
    try:
        with open('./moduls/evolution_analyser/progression.json', 'w') as file:
            json.dump(filejson, file)
    except Exception as _:
        pass
    
    return filejson['propagations']


def propagationsAnalyser() -> list:
    with open('./moduls/evolution_analyser/progression.json', 'r') as f:
        return json.load(f)['propagations']