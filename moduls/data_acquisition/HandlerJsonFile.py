import json
from .filterData import moisName
from moduls.data_loader.ReadJsonFile import get_declaration_from_filename
import traceback

PATH_JSON_FILE = './moduls/data_acquisition/json_files'


def add_declaration(declaration, overwise=True) -> bool:
    """
    Use for adding a new declaration to a json good file
    If the file matching to date_publication is then append new declaration
    otherwise create file and add new declaration
    """

    try:
        print("declaration add ", declaration)
        declaration['is_loaded'] = False
        declaration['nom'] = f"Declaration du {declaration['date_publication'].replace('/', ' ')}"
        filename = construct_filename(declaration['date_publication'])
        declarations = get_declaration_from_filename(filename)
        
        # testing if declaration already exists in json file
        
        data = dict()
        # means the file doesn't exist
        if (declarations is None) or (len(declarations) == 0):
            # then add new single declaration
            data['declarations'] = [declaration]
        else:  # means file exists and might content at least one declaration, then append new declaration
            if declaration_in(declaration, declarations) and ((overwise is True) or ((overwise is not None) and (not overwise()))):
                for i, element in enumerate(declarations):
                    if element['date_publication'] == declaration['date_publication']:
                        declarations[i] = declaration # overwise
            else:
                declarations.append(declaration)       # add new
            data['declarations'] = declarations

        with open(f'{PATH_JSON_FILE}/{filename}', 'w') as outputfile:
            json.dump(data, outputfile, indent=4, ensure_ascii=False)

        return True
    except Exception as _:
        print(traceback.print_exc())
        return False


def update_value_of_declaration(declaration) -> bool:
    """ Update just values of keys present in given declaration """

    try:
        # get name of file
        path = construct_filename(declaration['date_publication'])
        declarations = get_declaration_from_filename(path)

        for i in range(len(declarations)):
            if declarations[i]['date_publication'] == declaration['date_publication']:
                for key, value in declaration.items():
                    declarations[i][key] = value
                break

        # overwise the old data by the new data
        with open(path, 'w') as file:
            json.dump({'declarations': declarations}, file, indent=2)

        return True
    except Exception as e:
        print(e)
        return False


def set_loaded(date_publication: str) -> bool:
    """ Update just values of keys present in given declartion date """

    try:
        # get name of file
        filename = construct_filename(date_publication)
        declarations = get_declaration_from_filename(filename)

        for i in range(len(declarations)):
            if declarations[i]['date_publication'] == date_publication:
                declarations[i]['is_loaded'] = True # update only this property

                # overwise the old data by the new data
                with open(f"{PATH_JSON_FILE}/{filename}", 'w') as file:
                    json.dump({'declarations': declarations}, file, indent=2)
                
                return True # successful

        return False # not found

    except Exception as _:
        print(traceback.print_exc())
        return False


def update_declaration(declaration) -> bool:
    """ Update a declration on file """

    try:
        # get name of file
        path = construct_filename(declaration['date_publication'])
        declarations = get_declaration_from_filename(path)

        for i in range(len(declarations)):
            if declarations[i]['date_publication'] == declaration['date_publication']:
                declarations[i] = declaration
                break

        # overwise the old data by the new data
        with open(path, 'w') as file:
            json.dump({'declarations': declarations}, file, indent=2)

        return True
    except Exception as e:
        print(e)
        return False


def declaration_in(declaration: dict, declarations: list) -> bool:
    """ Check if declaration is in the declarations list """

    for element in declarations:
        if element['date_publication'] == declaration['date_publication']:
            return True

    return False


def construct_filename(date_publication: str) -> str:
    """
    Construct the path file from publication date (date_publication)
    Example: 2021/7/23 --> 2021_jouillet.json
    """

    year, month, _ = date_publication.split('/')

    month = moisName(month)

    return f"{year}_{month}.json"
