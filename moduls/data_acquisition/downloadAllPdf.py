import os
from bs4 import BeautifulSoup
import sys
import requests
import shutil
import traceback

from .treatPdf import getDeclarationFromPdf

global countPdfNumber
countPdfNumber = 0


def __downloadFromLink(pageLink, error=None):
    try:
        return _downloadPdfNew(pageLink)
    except Exception as e:
        if error is not None:
            error(e)
            print("link failure", traceback.print_exc())


def _downloadPdfNew(pageLink) -> dict:
    theCount = countPdfNumber
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    r = requests.get(pageLink, headers=headers)
    #print("printing response", r)
    open("temp.html", "wb").write(r.content)

    f = open("temp.html", "r")

    content = f.read()

    soup = BeautifulSoup(content, 'html.parser')

    HtmlSpan = soup.find('span', class_="file")

    if HtmlSpan is not None:
        downloadLink = HtmlSpan.find('a').attrs.get('href')
        result = requests.get(downloadLink, headers=headers)
        open('temp.pdf', 'wb').write(result.content)
        if not os.path.exists('./assets'):
            os.mkdir('./assets')
        shutil.move('temp.pdf', './assets/temp.pdf')
        print("test")
        declaration = getDeclarationFromPdf()
        declaration['url'] = pageLink
        return declaration
    return None


def _getAllLinkOnPage(page, next=None):
    url = 'http://www.sante.gouv.sn/Actualites?page='
    linksTab = []
    if os.path.exists('temp.html'):
        os.remove('temp.html')
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    try:
        r = requests.get(url+str(page), headers=headers)

        open("temp.html", "wb").write(r.content)

        page = open("temp.html", "r")
        pageContent = page.read()

        soup = BeautifulSoup(pageContent, 'html.parser')

        divField = soup.find_all('div', class_="small-12 medium-6 large-4")

        for x in divField:

            linksTab.insert(len(linksTab), 'http://www.sante.gouv.sn/' +
                            x.find('a').attrs.get('href'))

        '''if len(linksTab) > 0:
            f = _downloadPdfNew(linksTab[0], error)
            next(f, page)'''
        for x in linksTab:

            newDeclaration = _downloadPdfNew(x)
            print("newDeclaration in all links", newDeclaration)

            if(next is not None):
                next(newDeclaration)

        # links = divField.find_all('a')

        # print(links)

        #x.insert(len(x), valeur)
    except Exception as e:
        raise e


def launchAcquisitionModule(next=None, error=None, firstPage=0, lastPage=None) -> None:
    '''
        Next retourne None lorsque tous les pdf sont traités
    '''

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    try:
        r = requests.get(
            'http://www.sante.gouv.sn/Actualites', headers=headers)

        open("all.html", "wb").write(r.content)
        if not os.path.exists('assets'):
            os.mkdir('assets')

        f = open("all.html", "r")

        contentAll = f.read()

        soup = BeautifulSoup(contentAll, 'html.parser')

        liLastPager = soup.find('li', class_='pager-last last')

        lastPagerHref = liLastPager.find('a').attrs.get('href')

        if lastPage is None:
            for x in range(lastPagerHref.find('page=')+5, len(lastPagerHref)):
                lastPage = int(lastPagerHref[x])

        # for x in range(0, int(lastPage)):
        for x in range(firstPage, lastPage):
            try:
                _getAllLinkOnPage(x, next)
            except Exception as e:
                if error is not None:
                    error(x)
                raise e
        
        next(None) # finish

    except Exception as e:
        print(traceback.print_exc())

    '''if(next is not None):
        next(None)'''


# _downloadPdfNew('https://sante.sec.gouv.sn/Actualites/coronavirus-communiqu%C3%A9-de-presse-n%C2%B0403-du-jeudi-08-avril-2021-du-minist%C3%A8re-de-la-sant%C3%A9-et')
