try:
    from PIL import Image
except ImportError:
    import Image
import tempfile
import pytesseract
import os
from pdf2image import convert_from_path
import glob
import traceback

# convertir les pages du pdf en jpeg


def pdfToJpeg(filename):
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(
            filename, output_folder=path)
    #os.system("mkdir pagePdf")
    os.mkdir("pagePdf")
    os.chdir("pagePdf")
    for i, page in enumerate(images_from_path):
        page.save('page' + str(i) + '.jpg', 'JPEG')


# convertion image to text
def imageToText(filename):
    text = pytesseract.image_to_string(filename, lang="fra")
    return text


def traitementPdf(filename):
    pdfToJpeg(filename)
    image_list = []
    i = 0
    for filename in glob.glob('*.jpg'):
        '''im = Image.open('page' +str(i)+'.jpg')
        image_list.append(im)'''
        im = imageToText('page'+str(i)+'.jpg')
        image_list.append(im)
        i += 1
    'print(datefinder.find_dates(image_list))'
    os.chdir("..")
    os.system("rm -rf pagePdf")
    if len(image_list) >  1:
        images = image_list[0] + image_list[1]
    else:
        images = image_list[0]
    return images

def traitementImage(declarationId):
    imageData = None
    for i in range (0, len(declarationId)):
        imageData += imageToText(declarationId[i])

    return imageData
    
