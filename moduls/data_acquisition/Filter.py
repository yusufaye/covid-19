from moduls.data_acquisition.getDataFromPdf import imageToText
import os
from pdf2image import convert_from_path
import tempfile
from moduls.data_acquisition.filterData import *
import re
import shutil

class Filter:
    def __init__(self, pdfFile = None, imageUrl = None):
        self.pdfFile = pdfFile
        self.imageUrl = imageUrl
        if self.pdfFile is not None:
            self.imageUrl = []
            self.extractImage(pdfFile)
        

    def traitement(self):
        imageData = ""
        for index, image in enumerate(self.imageUrl):
            imageData += imageToText(image)
        return imageData

    def extractImage(self, filename):
        with tempfile.TemporaryDirectory() as path:
            images_from_path = convert_from_path(filename, output_folder=path)
        os.mkdir("pagePdf")
        for i, page in enumerate(images_from_path):
            imagePath = 'pagePdf/page' + str(i) + '.jpg'
            page.save(imagePath, 'JPEG')
            self.imageUrl.append(imagePath)

    def filterPrimary(self) -> list:
        """
        [
            {
                'nom': 'Fatick',
                'localites': [
                    {'nom': 'Dioffior', 'cas': 1},
                ]
            },
        ]
        """
        
        text = self.traitement() # get text from images
        text = text.replace('-\n', '-')
        text = text.replace(',\n', ', ')

        extracted = list()  # will containt all extracted villes and each localite (case)
        villes = Filter.get_locations() # read villes and its localites from Senegal.json

        # for each ville
        for item in villes:
            localites = list()
            for localite in item['localites']:
                cas = Filter.get_cas(localite, text)
                if cas is not None:
                    localites.append({
                        'nom': localite,
                        'cas': cas,
                    })

            # if the ville has at least appear once in the data, then add it
            if len(localites) > 0:
                extracted.append({
                    'nom': item['nom'],
                    'localites': localites
                })
        
        return extracted
        

    def filtrageNew(self):
        data = self.traitement()

        result = re.search('[a-zA-Z0-9]+', data)
        if not result:
            return None
        
        declaration = {}

        # récuperation de la date
        '''if(i == 0):
        date = getDate(self.traitement)
        print('date', date)
        defDate = date
        else:
        date = defDate'''

        date = getDate(data)

        # récupération du nombres de tests
        tests=findWordValue(data, 'tests')
        # recuperation de cas
        cas=findWordValue(data, 'cas')
        # recuperation des cas contacts
        contacts=findWordValue(data, 'contacts')
        if contacts == 0:
            contacts=findWordValue(data, 'contact')
        # recuperation des cas importés
        importes=findWordValue(data, 'importé')
        if importes == 0:
            importes=findWordValue(data, 'importés')

        # recuperation de patients
        patients=findWordValue(data, 'patients')
        # recuperation de cas graves
        graves=findWordValue(data, 'graves')
        # recuperation des décès
        deces=findWordValue(data, 'décès')

        #récupération départements dakar
        dakar = filtrageDakar(data)

        #récuperation régions
        villes = filtrageRegions(data)
        #regions = d.split(',')

        first = {
            'date_publication': date,
            'nom': "publication du " + str(date),
            'url': "",
            'nombre_tests': tests,
            'nombre_cas_positifs': cas,
            'nombre_cas_contacts': contacts,
            'nombre_cas_importes': importes,
            'nombre_patients': patients,
            'nombre_cas_graves': graves,
            'nombre_deces': deces
        }
        #villes = list(dakar).extend(villes)
        tmp = list()
        tmp.append(dakar)
        tmp.extend(villes)

        #ajout de la région de dakar dans le dictionnaire
        first.update({"villes" : tmp})

        #traitement des régions
        '''
        for region in regions:
            value=findWordValue(data, region)
            if value > 0:
                # ajout de chaque région trouvée dans le dictionnaire
                region={
                    region: {
                        "nombre_cas_communautaire": value}
                }
                first.update(region)
        '''

        declaration.update(first)
        shutil.rmtree('pagePdf')
        return declaration

    
    @classmethod
    def get_locations(cls) -> list:
        villes = list()
        
        with open('./moduls/data_acquisition/Senegal.json') as file:
            data = json.load(file)
            villes = list()

            for ville in data:
                try:
                    localites = set()
                    localites = list()
                    for localite in ville['departements']:
                        localites.append(localite['departement'].capitalize())
                        localites.extend([com.capitalize() for com in localite['communes']])
                        localites.extend([arrond.capitalize() for arrond in localite['arronds']])
                        localites.extend([comard.capitalize() for comard in localite['comard']])
                    v = {
                        'nom': ville['region'].capitalize(),
                        'localites': set(localites)
                    }
                    villes.append(v)
                except Exception as _:
                    pass

            return villes

    @classmethod
    def get_cas(cls, nom: str, data: str) -> int:
        lines = data.split('\n') # divid text to lines

        for (i, line) in enumerate(lines):
            result = re.search(nom, line, re.IGNORECASE)
            if result:
                index = i
                while index > 0:
                    cas = re.search(r'( )*[0-9]{1,3} ( )*', lines[index], re.IGNORECASE)
                
                    if cas:
                        return int(cas.group()) # return as cas
                    index -= 1

                return 1 # default, 1 cas    

        return None # not found


# filename = ['moduls/data_acquisition/tweeter/downloaded_media/1382274614244614144_image_1.jpg'
#             , 'moduls/data_acquisition/tweeter/downloaded_media/1382274614244614144_image_2.jpg']


# filter = Filter(imageUrl=filename)

# for extract in filter.filterPrimary():
#     print(extract)
