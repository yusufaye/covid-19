from moduls.data_acquisition.tweeter.TweepyManager import TweepyManager
from moduls.data_acquisition.Filter import Filter
import requests
from bs4 import BeautifulSoup
import os
import shutil

class Downloader:

    @classmethod
    def downloadFromUrl(cls, url: str, onDownload=print) -> dict:
        """
        1.  From url, download file from given url
        2.  Extract and return data from file downloaded
        """

        #theCount = countPdfNumber
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        r = requests.get(url, headers=headers)
        #print("printing response", r)
        open("temp.html", "wb").write(r.content)

        f = open("temp.html", "r")

        content = f.read()

        soup = BeautifulSoup(content, 'html.parser')

        HtmlSpan = soup.find('span', class_="file")

        if HtmlSpan is not None:
            downloadLink = HtmlSpan.find('a').attrs.get('href')
            result = requests.get(downloadLink, headers=headers)
            open('temp.pdf', 'wb').write(result.content)
            if not os.path.exists('./assets'):
                os.mkdir('./assets')
            shutil.move('temp.pdf', './assets/temp.pdf')

            onDownload() # inform that download done

            # create a new instance to trait data from temp.pdf file
            _filter = Filter(pdfFile='./assets/temp.pdf')
            declaration = _filter.filtrageNew() # get declaration dict

            declaration['url'] = url # add a missing attribut

            return declaration  # return download

        return None # error when download


    @classmethod
    def downloadAllTweets(cls, max_id=None, onDownload=print, onFilter=print) -> list:
        """
        1. Download a list of tweets from tweeter API
        2. Extract data and return a list of declaration from the downloaded images files
        """
        
        print('MAX_ID >>>>>', max_id)
        tweetpy = TweepyManager(max_id=max_id)
        tweetpy.search()
        tweets = tweetpy.save_image(notify=onDownload)
        onDownload(None)    # notified when download is done
        declarations = []
        
        for tweet in tweets:
            images = [image['file_name'] for image in tweet['images']]
            _filter = Filter(imageUrl=images)
            data = _filter.filtrageNew()
            if data is None:
                print(' :( No DATA from filter method')
                continue 
            else:
                data['url'] = tweet['tweet_id'] # get tweet_id as url
                declarations.append(data)
                onFilter(data)  # notified for each 
    
        onFilter(None)      # notified that all filter is done
        
        return declarations
