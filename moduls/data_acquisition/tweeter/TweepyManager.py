import traceback
import urllib
import os
import json
import tweepy as tw
from traceback import print_exc
from paths import PATH_DATA_ACQUISITION

PATH = f'{PATH_DATA_ACQUISITION}/tweeter'

class TweepyManager():
    """ CLASS USED TO HANDLE TWEETER API """

    credentials = {
        "consumer_key": "W9q8vDeSbs1E7wFqPkuffdA7H",
        "consumer_secret": "J5oFLSAXFPEmISxUMDgpFTb3DHMvK1BrmbG0ZE3hIQ5ZFjUGWz",
        "access_token": "1384819046730842116-3HU0RZSfjzH18iskXvuZHbmJUMzdRD",
        "access_token_secret": "BBfiRbFH4QEHXtnFMswEpMiAanhFh5U27LuF2iIM99Eak"
    }

    api = None  # the link from our app to tweeter api

    def __init__(self,
                 search_words="MinisteredelaS1 #Cov19sn -filter:retweets",
                 date_since='2020-03-01',
                 max_id=None,
                 count=150,
                 ) -> None:
        self.search_words = search_words
        self.max_id = max_id
        self.date_since = date_since    # start date
        self.count = count              # The number of tweets to return per page

        self.tweets = None              # will contains all tweets

    @classmethod
    def get_api(cls):
        """ Make a instance of api to communicate with tweeter api """

        if cls.api is None:
            consumer_key = cls.credentials["consumer_key"]
            consumer_secret = cls.credentials["consumer_secret"]
            access_token = cls.credentials["access_token"]
            access_token_secret = cls.credentials["access_token_secret"]

            auth = tw.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
            cls.api = tw.API(auth, wait_on_rate_limit=True)

        return cls.api

    def search(self, words: str = None) -> bool:
        """ Make a search """

        # update property words
        if words is not None:
            self.search_words = words

        if self.max_id is not None:
            cursor = tw.Cursor(self.get_api().search,
                                q=self.search_words,
                                screen_name='MinisteredelaS1',
                                # rpp=5,
                                lang="fr",
                                since=self.date_since,
                                # count=self.count,
                                max_id=self.max_id,
                                tweet_mode="extended",
                                show_user=True)
        else:
            cursor = tw.Cursor(self.get_api().search,
                                q=self.search_words,
                                screen_name='MinisteredelaS1',
                                # rpp=5,
                                lang="fr",
                                since=self.date_since,
                                # count=self.count,
                                tweet_mode="extended",
                                show_user=True)
        # Collect tweets
        self.tweets = cursor.items(self.count)

    def save_image(self, folder: str = f'{PATH}/downloaded_media', notify=print) -> list:
        """
        Save all image containts by each result
        Example: [{
                'tweet_id': 1384813311020179457,
                'full_text': 'Communiqué 416/21 Avril,
                'images': [
                    {
                        'media_id': '13848132493131509378',
                        'media_url': 'http://pbs.twimg.com/media/EzfYUC2WEAIJphx.jpg',
                        'file_name': '1384813311020179457_image_1.jpg'
                    },
                    {
                        'media_id': '1384813292565188611',
                        'media_url': 'http://pbs.twimg.com/media/EzfYWj-WUAMNYRr.jpg',
                        'file_name': '1384813311020179457_image_2.jpg'}]
                    }
                ]
            }]
        """

        # keep track of IDs of downloaded images to avoid re-downloads
        downloaded_img_ids = []
        media_tweets = []  # will containt all medias
        for tweet in self.tweets:
            # will look like {twee_id: 5, images: [5_image_1, ...]}
            media_tweet = dict()
            media_tweet['tweet_id'] = tweet.id
            media_tweet['full_text'] = tweet.full_text
            media_tweet['images'] = []

            try:
                if 'media' in tweet.extended_entities:
                    i = 1  # initialize i
                    for media in tweet.extended_entities['media']:
                        media_id = str(media['id'])
                        media_url = media['media_url']
                        if not(media_id in downloaded_img_ids):  # don't re-download images

                            # something like 51218_image_1.jpg
                            file_name = f'{tweet.id}_image_{i}{os.path.splitext(media_url)[1]}'
                            image = {
                                'media_id': media_id,
                                'media_url': media_url,
                                'file_name': f'{folder}/{file_name}',
                            }
                            media_tweet['images'].append(image)
                            i += 1

                            urllib.request.urlretrieve(
                                media_url, os.path.join(folder, file_name))

                            downloaded_img_ids.append(media_id)

                    # append new media
                    media_tweets.append(media_tweet)
                    notify(media_tweet)


            except Exception as _:
                print(print_exc())

        # collect all id of tweets and save them
        tweet_ids = [media['tweet_id'] for media in media_tweets]
        TweepyManager.add_tweet_ids(tweet_ids) # save all ids into tweeters.json file

        return media_tweets

    @classmethod
    def add_tweet_ids(cls, ids: list) -> bool:
        """ Use to add new key on tweets.json file """

        try:
            with open(f'{PATH}/tweets.json', 'r') as file:
                olds = json.load(file)

            olds['tweets'].extend(ids)
            olds['tweets'] = set(olds['tweets'])
            olds['tweets'] = list(olds['tweets'])
            with open(f'{PATH}/tweets.json', 'w') as file:
                json.dump(olds, file, indent=4)

            return True

        except Exception as _:
            print(traceback.print_exc())
            return False

    
    @classmethod
    def last_tweet_id(cls) -> int:
        """ Use for getting the last id of which downloaded tweets """

        with open(f'{PATH}/tweets.json', 'r') as file:
            ids = json.load(file)['tweets']

            if len(ids) > 0:
                return min(ids)

        return 0
