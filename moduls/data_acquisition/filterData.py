import json
from os.path import join
#from moduls.data_loader.ReadJsonFile import PATH_JSON_DATA, get_declaration_from_filename


def filtrage(data):
    declaration = {}

    # récuperation de la date
    '''if(i == 0):
      date = getDate(data)
      print('date', date)
      defDate = date
    else:
      date = defDate'''
    date = getDate(data)


    #declaration['Declaration'] = {}


    # récupération du nombres de tests
    tests=findWordValue(data, 'tests')
    # recuperation de cas
    cas=findWordValue(data, 'cas')
    # recuperation des cas contacts
    contacts=findWordValue(data, 'contacts')
    if contacts == 0:
        contacts=findWordValue(data, 'contact')
    # recuperation des cas importés
    importes=findWordValue(data, 'importé')
    if importes == 0:
        importes=findWordValue(data, 'importés')

    # recuperation de patients
    patients=findWordValue(data, 'patients')
    # recuperation de cas graves
    graves=findWordValue(data, 'graves')
    # recuperation des décès
    deces=findWordValue(data, 'décès')

    #récupération départements dakar
    dakar = filtrageDakar(data)

    #récuperation régions
    d = filtrageRegions(data)
    #regions = d.split(',')

    first = {
        'date_publication': date,
        'nom': "publication du " + str(date),
        'url': "",
        'nombre_tests': tests,
        'nombre_cas_positifs': cas,
        'nombre_cas_contacts': contacts,
        'nombre_cas_importes': importes,
        'nombre_patients': patients,
        'nombre_cas_graves': graves,
        'nombre_deces': deces
    }

    villes = []

    villes.append(dakar)
    villes.append(d)

    #ajout de la région de dakar dans le dictionnaire
    first.update({"villes" : villes})

    declaration.update(first)
    print(declaration)
    #return declaration

def filtrageNew(data):

    '''global defDate
    defDate = ""'''
    declaration = {}

    # récuperation de la date
    '''if(i == 0):
      date = getDate(data)
      print('date', date)
      defDate = date
    else:
      date = defDate'''
    date = getDate(data)


    #declaration['Declaration'] = {}


    # récupération du nombres de tests
    tests=findWordValue(data, 'tests')
    # recuperation de cas
    cas=findWordValue(data, 'cas')
    # recuperation des cas contacts
    contacts=findWordValue(data, 'contacts')
    if contacts == 0:
        contacts=findWordValue(data, 'contact')
    # recuperation des cas importés
    importes=findWordValue(data, 'importé')
    if importes == 0:
        importes=findWordValue(data, 'importés')

    # recuperation de patients
    patients=findWordValue(data, 'patients')
    # recuperation de cas graves
    graves=findWordValue(data, 'graves')
    # recuperation des décès
    deces=findWordValue(data, 'décès')

    #récupération départements dakar
    dakar = filtrageDakar(data)

    #récuperation régions
    villes = filtrageRegions(data)
    #regions = d.split(',')

    first = {
        'date_publication': date,
        'nom': "publication du " + str(date),
        'url': "",
        'nombre_tests': tests,
        'nombre_cas_positifs': cas,
        'nombre_cas_contacts': contacts,
        'nombre_cas_importes': importes,
        'nombre_patients': patients,
        'nombre_cas_graves': graves,
        'nombre_deces': deces
    }
    #villes = list(dakar).extend(villes)
    tmp = list()
    tmp.append(dakar)
    tmp.extend(villes)

    #ajout de la région de dakar dans le dictionnaire
    first.update({"villes" : tmp})

    #traitement des régions
    '''
    for region in regions:
        value=findWordValue(data, region)
        if value > 0:
            # ajout de chaque région trouvée dans le dictionnaire
            region={
                region: {
                    "nombre_cas_communautaire": value}
            }
            first.update(region)
    '''

    declaration.update(first)
    return declaration



# récupération de chaque zone de dakar + son nombre de cas communautaires

def filtrageDakar(data) -> dict:
    d = recupDepartementsDakar(data)
    depart = d.split(',')
    dakar = {}
    localites = []
    for zone in depart[:-1]:
        zone = zone.strip()
        if zone[-1] == ",":
            zone.replace(",", "")
        value = findWordValue(data, zone.split()[0])
        localites.append({
            "nom": zone.capitalize(),
            "cas": value
        })

    dakar = {
        "nom": "Dakar",
        "localites": localites
    }
    return dakar


def filtrageRegions(data) -> dict:
    d = recupDepartementsRegions(data)
    # print('raw data: ', d)
    localites = d.split(',')
    villes = []
    for zone in localites[:-1]:
        zone = zone.strip()
        if zone[-1] == ",":
            zone.replace(",", "")
        if len(zone) > 1:
            value = findWordValue(data, zone.split()[0])
            villes.append(
                {
                    "nom": zone.capitalize(),
                    "localites": [{
                        "nom": zone.capitalize(),
                        "cas": value
                    }]
                }
            )

    return villes


def findWordValue(sentence, key):
    words=str.split(sentence)
    j=0
    word=""
    for i in range(len(words)):
        if words[i][len(words[i]) - 1] == ',' or words[i][len(words[i]) - 1] == '.':
            word=words[i][:-1]
            if word == key:
                break
        else:
            if words[i] == key:
                break
        j += 1
        value=0
    try:
        for p in range(j, 0, -1):
            if words[p].isdigit():
                value=int(words[p])
                break
            if words[p].lower() == 'aucun':
                break
    except IndexError:
        value=0
    return value


# récupere la date du communiqué
def getDate(sentence):
    words = str.split(sentence)
    a = []
    for i in range(len(words)):
        if words[i] == 'Ce':
            for j in range(i + 2, i + 5):
                a.append(words[j])
            break
    date = str(a[2][:-1]) + '/' + str(mois(a[1])) + '/' + str(a[0])
    return date

   


def mois(mois):
    if len(mois) > 4:
        mois=mois[0:4]
    switcher={
        'janv': 1,
        'févr': 2,
        'fevr': 2,
        'mars': 3,
        'avri': 4,
        'mai': 5,
        'juin': 6,
        'juil': 7,
        'aout': 8,
        'août': 8,
        'sept': 9,
        'octo': 10,
        'nove': 11,
        'dece': 12,
        'déce': 12
    }
    return switcher.get(mois.lower(), "Invalide")


def moisName(mois) -> str:
    switcher={
        1: 'janvier',
        2: 'février',
        3: 'mars',
        4: 'avril',
        5: 'mai',
        6: 'juin',
        7: 'juillet',
        8: 'août',
        9: 'septembre',
        10: 'octobre',
        11: 'novembre',
        12: 'décembre'
    }
    return switcher.get(int(mois), "Invalide")


# récupere la position du mot
def findWord(sentence, key):
    words=str.split(sentence)
    j=0
    for i in range(len(words)):
        if words[i] == key:
            a=words[i]
            print("found", "position = ", j)
            break
        j += 1


# récupere les départements de dakar
def recupDepartementsDakar(sentence):
    words=str.split(sentence)
    j=0
    t=0
    # récuperer les zones de dakar
    for i in range(len(words)):
        if words[i].lower() == 'dont':
            break
        j += 1
    for i in range(len(words)):
        if words[i].lower() == 'régions':
            break
        t += 1
    depart=""
    a=""
    for i in range(j + 2, t):
        if (len(words[i]) > 3):
            if words[i] == a:
                continue
            elif words[i][len(words[i]) - 1] != ',' and words[i][len(words[i]) - 1] != '.':
                if words[i + 1] == ';':
                    depart += words[i] + ','
                elif words[i + 1].isdigit():
                    depart += words[i] + ' ' + words[i + 1] + ','
                elif words[i + 1][len(words[i + 1]) - 1] == ',' or words[i + 1][len(words[i + 1]) - 1] == '.':
                    depart += words[i] + ' ' + words[i + 1][:-1] + ','
                    a=words[i + 1]
                elif words[i + 2] == 'et' or words[i + 2] == '.' or words[i + 2] == ';':
                    depart += words[i] + ' ' + words[i + 1] + ','
                    a=words[i + 1]
            else:
                depart += words[i][:- 1] + ', '

    return depart[:-1]


# récupere toutes les régions sauf dakar
def recupDepartementsRegions(sentence):
    words=str.split(sentence)
    j=0
    t=0
    # récuperer les zones des régions
    for i in range(len(words)):
        if words[i].lower() == 'régions':
            break
        j += 1
    for i in range(len(words)):
        if words[i].lower() == 'patients':
            break
        t += 1
    depart=""
    a=""
    for i in range(j + 2, t):
        if (len(words[i]) > 3):
            if words[i] == a:
                continue
            elif words[i][len(words[i]) - 1] != ',' and words[i][len(words[i]) - 1] != '.':
                if words[i + 1] == ';' or words[i + 1] == '-':
                    depart += words[i] + ','
                elif words[i + 1].isdigit():
                    depart += words[i] + ' ' + words[i + 1] + ','
                elif words[i + 1][len(words[i + 1]) - 1] == ',' or words[i + 1][len(words[i + 1]) - 1] == '.' or words[i + 1][len(words[i + 1]) - 1] == ';':
                    depart += words[i] + ' ' + words[i + 1][:-1] + ','
                    a=words[i + 1]
                elif words[i + 2] == 'et' or words[i + 2] == '.' or words[i + 2] == ';':
                    depart += words[i] + ' ' + words[i + 1] + ','
                    a=words[i + 1]
            else:
                depart += words[i][:- 1] + ', '

    return depart[:-1]


'''nltk.download('punkt')'''

def recupDataZone(data):
    words = str.split(data)
    j = 0
    t = 0
    zones = []
    # récuperer les zones des régions
    for i in range(len(words)):
        if words[i] == 'communautaire':
            break
        j += 1
    for i in range(len(words)):
        if words[i] == 'patients':
            break
        t += 1

    for i in range (j + 3, t):
        if len(words[i]) > 3:
            if words[i].find('(') > -1 or words[i].find(")") > -1:
                #print("iteration", i, "mot", words[i + 1])
                zone = {"nom" : words[i - 1], "cas" : words[i][1:3].replace(')', '')}
                zones.append(zone)
            else:
                continue

    print(zones)
