import datetime

#from database.ville import Ville
from database.localite import Localite
from database.transmission_communautaire import TransmissionCommunautaire
import matplotlib as plt

class Explorer:
    def __init__(self, nomVille = "Dakar"):
        self.info_ville = self.get_info_ville(nomVille)

    @classmethod
    def get_info_ville(cls, nom):
        #recuperer les localités de la ville
        localites = Localite.findLocaliteByVille(nom)

        #recuperer la derniere declaration de chaque localite
        declaration = list()
        for localite in localites:
            data = TransmissionCommunautaire.queryWithLocalite(localite.id)
            declaration.append(data)

        return declaration

    @classmethod
    def plotData(cls, localite):
        data = TransmissionCommunautaire.queryWithLocalite(localite)
        x_values = [datetime.datetime.strptime(d, "%d/%m/%Y").date() for d in data["declaration_date_publication"]]
        #ax = plt.gca()
        y_values = data["declaration_nombre_cas_positif"]
        plt.plot(x_values, y_values)
