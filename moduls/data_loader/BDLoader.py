# o	Exportation directe sans attente vers Oracle
# o  o	Exportation indirecte avec les phases prépare et validation/annulation
import time
from database.declaration import Declaration
from moduls.data_loader.SimpleLoader import loader
from database.database import db

def AutoPushInBD(directcommit=False):
    #db = cx_Oracle.connect('SYS', 'passer123', dsn_tns, cx_Oracle.SYSDBA)
    #cursor=db.cursor()
    if directcommit==True:
        AllClass=loader()
        i=0
        #commit declaration
        dcl=AllClass[0]
        while i < len(dcl):
            print(dcl[i])
            #dcl[i].save()
            db.commit()
            i+=1
        print('commit declaration....')
        time.sleep(1)
        #commit region
        reg=AllClass[1]
        i=0
        while i < len(reg):
            print(reg[i])
            #reg[i].save()
            db.commit()
            i+=1
        print('commit region....')
        time.sleep(1)
        #commit departement
        dep=AllClass[2]
        i=0
        while i < len(dep):
            print(dep[i])
            dep[i].save()
            db.commit()
            i+=1
        print('commit departement....')
        time.sleep(1)
        #commit Transmission communautaire
        tcm=AllClass[3]
        i=0
        while i < len(tcm):
            print(tcm[i])
            tcm[i].save()
            db.commit()
            i+=1
        print('commit Transmission communautaire...')
        time.sleep(1)
    else:
        print("commit Prepare")