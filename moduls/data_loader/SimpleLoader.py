# o	Exportation directe sans attente vers Oracle
# o  o	Exportation indirecte avec les phases prépare et validation/annulation
from numpy.core.numeric import ones
from database.transmission_communautaire import TransmissionCommunautaire
from database.localite import Localite
from database.ville import Ville
import time
from database.declaration import Declaration
from moduls.data_loader.SimpleLoader import loader
from database.oracle import OracleDataBase


def AutoPushInBD(directcommit=False):
    #db = cx_Oracle.connect('SYS', 'passer123', dsn_tns, cx_Oracle.SYSDBA)
    # cursor=OracleDataBase.db.cursor()
    if directcommit == True:
        AllClass = loader()
        i = 0
        # commit declaration
        dcl = AllClass[0]
        while i < len(dcl):
            print(dcl[i])
            # dcl[i].save()
            OracleDataBase.db.commit()
            i += 1
        print('commit declaration....')
        time.sleep(1)
        # commit region
        reg = AllClass[1]
        i = 0
        while i < len(reg):
            print(reg[i])
            # reg[i].save()
            OracleDataBase.db.commit()
            i += 1
        print('commit region....')
        time.sleep(1)
        # commit departement
        dep = AllClass[2]
        i = 0
        while i < len(dep):
            print(dep[i])
            dep[i].save()
            OracleDataBase.db.commit()
            i += 1
        print('commit departement....')
        time.sleep(1)
        # commit Transmission communautaire
        tcm = AllClass[3]
        i = 0
        while i < len(tcm):
            print(tcm[i])
            tcm[i].save()
            OracleDataBase.db.commit()
            i += 1
        print('commit Transmission communautaire...')
        time.sleep(1)
    else:
        print("commit Prepare")


def save_declaration(data, autocommit=False):
    """ Save declaration from dict to oracle database """

    declaration = Declaration()
    declaration.nom = data['nom']
    declaration.nombre_test = data['nombre_test']
    declaration.nombre_cas_positif = data['nombre_cas_positif']
    declaration.nombre_cas_contact = data['nombre_cas_contact']
    declaration.nombre_cas_importe = data['nombre_cas_importe']
    declaration.date_publication = data['date_publication']

    declaration.save()  # the declaration is now saved

    # get all ville in data
    data_by_ville = map(lambda e: e['ville'], data['villes'])

    localites = Localite.query()
    villes = Ville.query()

    for cas_in_ville in data_by_ville:
        for cas_by_localite in cas_in_ville['localite']:
            cas = TransmissionCommunautaire()

            cas.declaration = declaration
            cas.localite = local_by_name(
                cas_by_localite['name'], localites, cas_in_ville['name'], villes)
            cas.nombre_cas = cas_by_localite['nombre_cas']
            cas.save()  # save the new transmission communautaire

    if autocommit:
        OracleDataBase.db.commit()


def local_by_name(localName: str, localites: list, villeName: str, villes: list):

    local = [local for local in localites if local['name'] == localName]

    if len(local) > 0:
        return local.pop(0)
    else:
        localite = Localite(nom=localName)
        ville = [ville for ville in villes if ville['name'] == villeName]
        if len(ville) > 0:
            localite.ville = ville.pop(0)
        else:
            ville = Ville(nom=villeName)
            ville.save()
            localite.ville = ville

    return localite  # return localite

# save All declaration
def save_all_declaration(dict_dlc,autocommit=False):
    save = False;
    for dcl__ in dict_dlc:
        # all dcl save
        save_declaration(dcl__ , autocommit)
        time.sleep(1)
        save = True
    return save
        
