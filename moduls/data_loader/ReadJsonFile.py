from os import listdir
from os.path import isfile, join
import json

PATH_JSON_DATA = './moduls/data_acquisition/json_files'


def get_all_filename() -> list:
    """ Return all name files """

    return [f for f in listdir(PATH_JSON_DATA) if isfile(join(PATH_JSON_DATA, f)) and join(PATH_JSON_DATA, f).endswith('.json')]


def get_files_declarations() -> tuple:
    """ Return all titles of files and its declarations """

    jsonPaths = get_all_filename()

    data = list()

    for jsonPath in jsonPaths:
        data.append(get_declaration_from_filename(jsonPath))

    files = [name.replace('.json', '').replace('_', ' ').upper()
             for name in jsonPaths]

    return (files, data)


def get_declaration_from_filename(filename: str) -> list:
    """ Return list of declaration corresponding to the given filename """

    if not filename.endswith('.json'):
        filename = f"{filename}.json"

    try:
        with open(join(PATH_JSON_DATA, filename), 'r') as file:
            content = json.load(file)

            declarations = [item for item in content['declarations']]

            return declarations
    except FileNotFoundError as e:
        print(e)
        return list()


def get_all_declarations():
    """ Return all declarations """

    # will content all declarations
    data = list()

    for jsonfile in get_all_filename():
        [data.append(declaration)
            for declaration in get_declaration_from_filename(jsonfile)]

    # return all declarations
    return data


def find_declaration_by_name_and_file(file_name: str, date_declaration: str):
    file_name = file_name.replace(' ', '_').lower()

    for declaration in get_declaration_from_filename(file_name):
        if declaration['date_publication'] in date_declaration:
            return declaration

    return None
