# o	Exportation directe sans attente vers Oracle
# o  o	Exportation indirecte avec les phases prépare et validation/annulation
# from numpy.core.numeric import ones
from database.mysql_connector import MySQLDataBase
from database.transmission_communautaire import TransmissionCommunautaire
from database.localite import Localite
from database.ville import Ville
from database.declaration import Declaration
from moduls.data_acquisition.HandlerJsonFile import set_loaded
import traceback


def save_all_declarations(elements: list, autocommit=False, callback=print) -> bool:
    """ SAVE A LIST OF DECLARATION """

    if len(elements) > 0:
        # trait each item
        for item in elements:
            # save and get saved declaration
            declaration = save_declaration(item)

            # abandonne all saving data
            if declaration is None:
                MySQLDataBase.db.rollback()  # abort all changes
                return False  # save error

            # emit saved element
            if callback is not None:
                # emit both declaration dict and declaration object of Declaration
                callback(item, declaration)

        # accept changes if autocommit and if all elements have saved successfully
        if autocommit:
            for item in elements:
                set_loaded(item['date_publication'])  # update property 'is_loaded' to True
            MySQLDataBase.db.commit()  # commit changes in Mysql database
        
        return True  # successful saved
    else:
        return False

def save_declaration(data: dict) -> Declaration:
    """ Save declaration from dict to oracle database """

    declaration = Declaration()
    declaration.date_publication = data['date_publication']
    declaration.nom = data['nom']
    declaration.nombre_tests = data['nombre_tests']
    declaration.nombre_cas_positifs = data['nombre_cas_positifs']
    declaration.nombre_cas_contacts = data['nombre_cas_contacts']
    declaration.nombre_cas_importes = data['nombre_cas_importes']
    declaration.nombre_cas_graves = data['nombre_cas_graves']
    declaration.nombre_patients = data['nombre_patients']
    declaration.nombre_deces = data['nombre_deces']
    try:
        declaration.url = data['url']
    except Exception as _:
        pass

    try:
        declaration.save()  # try to save

        for cas_in_ville in data['villes']:
            for cas_by_localite in cas_in_ville['localites']:
                cas = TransmissionCommunautaire()
                localite = get_local(
                    cas_by_localite['nom'].capitalize(), cas_in_ville['nom'].capitalize())

                cas.localite = localite
                cas.declaration = declaration
                cas.nombre_cas = cas_by_localite['cas']
                cas.save()  # save the new transmission communautaire

        return declaration  # successfully

    except Exception as _:
        print(traceback.print_exc())

        return None  # ERROR


def commit_changes(elements: list) -> bool:
    """ Commit changes and update property 'is_loaded' for each items """

    try:
        # trait each items
        for item in elements:
            set_loaded(item['date_publication'])  # update property 'is_loaded'

        MySQLDataBase.db.commit()  # commit changes
        return True

    except Exception as _:
        print(traceback.print_exc())
        return False


def get_local(localName: str, villeName: str):
    """
    Get local if exists or return a new local
    """

    local = Localite.find_by_nom(localName)  # try to found local from database

    if local is not None:
        return local  # return found local
    else:
        # we must create new local with given name
        local = Localite(nom=localName)  # new instance of local

        ville = Ville.find_by_nom(villeName)  # try to found a ville
        if ville is not None:
            local.ville = ville
        else:
            ville = Ville(nom=villeName)  # new instance of ville
            ville.save()  # save a new ville
            local.ville = ville
        local.save()  # save a new local linked with ville

        return local  # return new created local


# def get_local(localName: str, localites: list, villeName: str, villes: list):
#     print('localName')
#     print('localites')
#     print('villeName')

#     local = [local for local in localites if local.nom == localName]

#     if len(local) > 0:
#         return local.pop(0)
#     else:
#         localite = Localite(nom=localName)
#         ville = [ville for ville in villes if ville.nom == villeName]
#         if len(ville) > 0:
#             localite.ville = ville.pop(0)
#         else:  # create new ville
#             ville = Ville(nom=villeName)
#             ville.save()
#             localite.ville = ville

#         localite.save()  # save new local

#     return localite  # return localite
